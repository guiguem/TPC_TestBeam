#include <stdio.h>
#include <sys/time.h>
#include <assert.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include "Event.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TF1.h"
#include "TFile.h"
#include "TTree.h"

#include "geomManager.hh"

using namespace std;

bool firstEvent = true;

// TODO declare the output
TFile* file;
TTree* DataTree;
TTree* GeomTree;
vector<short>          listOfPadChannels;
vector<vector<short> > listOfPadSamples;

#include <iostream>

#ifdef HAVE_MIDAS
#include "TMidasOnline.h"
#endif
#include "TMidasEvent.h"
#include "TMidasFile.h"

#ifdef HAVE_ROOT_XML
#include "XmlOdb.h"
#endif

std::vector <TH1F*> listofhistos;
std::vector <int> idchannel;

#include "VirtualOdb.h"

#include <vector>

VirtualOdb* gOdb = NULL;

int  gEventCutoff = 0;
bool gSaveOdb = false;
bool gPrintBank = false;

bool Decode(char *buffPtr,int buffSize, Event &event, bool debug=false );

void HandleMidasEvent(TMidasEvent& event) {

  //int eventId = event.GetEventId();

  // clear the output vectors
  listOfPadChannels.clear();
  for (uint i = 0; i < listOfPadSamples.size(); ++i)
    listOfPadSamples[i].clear();
  listOfPadSamples.clear();

  const char* fBank = "TR00";
  int banklength = 0;
  int banktype = 0 ;
  char *bankPtr;

  if( event.FindBank(fBank,&banklength,&banktype,(void**)&bankPtr) ) {

    banklength -=1;

    banklength *= sizeof(DWORD);

    bankPtr += sizeof(int)/sizeof(char);

    //   std::cout << " Found TR00 with length " << banklength << " Type " << banktype << std::endl;

    Event event;

    Decode(bankPtr,banklength,event,false);

    // loop over pads
    for(unsigned int i = 0; i < event.Pulse.size(); i++ ) {
      int channel = event.Pulse[i].Channel;

      TH1I *lwf = new TH1I("test", "", 520, 0., 520);

      vector<short> tmp_short;

      int adcmax = 0;

      // loop over ADC vs time
      for(unsigned int j = 0; j < event.Pulse[i].Time.size(); j++ ) {
        if (event.Pulse[i].Time[j] < 5 || event.Pulse[i].Time[j] > 490)
          continue;
        int adc = event.Pulse[i].ADC[j] - 255;

        lwf->Fill(event.Pulse[i].Time[j], adc);
        if (adc > adcmax)
          adcmax = adc;

      } // loop

      for (Int_t j = 0; j < lwf->GetNbinsX(); ++j) {
        tmp_short.push_back((short)lwf->GetBinContent(j));
      }

      delete lwf;
      listOfPadChannels.push_back((short)channel);
      listOfPadSamples.push_back(tmp_short);
    } // loop over pads

    if (firstEvent && event.Pulse.size() > 0) {
      cout << "First event compression 1/0: " << event.compress << endl;
      firstEvent = false;
    }

  } else if (gPrintBank)
    event.Print("a");
  else
    event.Print();

  // Fill the output
  DataTree->Fill();
}

int ProcessMidasFile(const char*fname, const char*output_name) {
  TMidasFile f;
  bool tryOpen = f.Open(fname);

  int run;

  char *s = strchr((char*)fname,'0');

  sscanf(s,"%d.mid",&run);

  std::cout << run << std::endl;

  if (!tryOpen) {
    printf("Cannot open input file \"%s\"\n",fname);
    return -1;
  }

  int numberofevents = 0;

  // output file
  file = new TFile(output_name,"RECREATE");

  ///// setup geometry
  femGeomManager geomManager;
  string electronicMap = "etc/TPCLPlayout.txt";
  string geometryMap   = "etc/TPClayout.txt";
  geomManager.setElectronicMap(electronicMap);
  geomManager.setGeometryMap(  geometryMap  );

  ////// dump geometry to output file
  geomManager.dumpToRootFile(file);
  file->cd();

  cout << "Geometry is written into output file" << endl;


  DataTree = new TTree("padData", "tree with data samples");

  DataTree->Branch("PadphysChannels", &listOfPadChannels );
  DataTree->Branch("PadADCvsTime"   , &listOfPadSamples );

  int i=0;
  while (1) {
    TMidasEvent event;
    if (!f.Read(&event))
      break;

    int eventId = event.GetEventId();
    //printf("Have an event of type %d\n",eventId);
    //      if( numberofevents > 1 ) break;
    numberofevents++;

    if ((eventId & 0xFFFF) == 0x8000) {
      // begin run
      event.Print();

      if (gSaveOdb) {
        char fname[256];

        char* ptr = event.GetData();
        int size = event.GetDataSize();

        if (strncmp(ptr, "<?xml", 5) == 0)
          sprintf(fname,"odb%05d.xml", event.GetSerialNumber());
        else
          sprintf(fname,"odb%05d.odb", event.GetSerialNumber());

        FILE* fp = fopen(fname,"w");
        if (!fp) {
          fprintf(stderr,"Error: Cannot write ODB to \'%s\', errno %d (%s)\n", fname, errno, strerror(errno));
          exit(1);
        }

        fwrite(ptr, size, 1, fp);
        fclose(fp);

        fprintf(stderr,"Wrote ODB to \'%s\'\n", fname);
        exit(0);
      }

   //
   // Load ODB contents from the ODB XML file
   //
   if (gOdb)
    delete gOdb;
       gOdb = NULL;
#ifdef HAVE_ROOT_XML
   gOdb = new XmlOdb(event.GetData(),event.GetDataSize());
#endif
  }
    else if ((eventId & 0xFFFF) == 0x8001)
  {
   // end run
   event.Print();
  }
    else if ((eventId & 0xFFFF) == 0x8002)
  {
   // log message
   event.Print();
   printf("Log message: %s\n", event.GetData());
  }
    else
  {
   event.SetBankList();
   //event.Print();
   HandleMidasEvent(event);
  }

    if((i%500)==0)
  {
   printf("Processing event %d\n",i);
  }

    i++;

    if ((gEventCutoff!=0)&&(i>=gEventCutoff))
  {
   printf("Reached event %d, exiting loop.\n", i);
   break;
  }
   }

  f.Close();

  // Write the output and close the file
  file->cd();
  DataTree->Write("", TObject::kOverwrite);
  cout << "Data sample data was written into output file" << endl;
  cout << "Nevents: " << i << endl;

  file->Close();

  return 0;
}

#ifdef HAVE_MIDAS

void startRun(int transition,int run,int time)
{
  printf("Begin run: %d\n", run);
}

void endRun(int transition,int run,int time)
{
  printf("End of run %d\n",run);
}

#endif

void help()
{
  printf("\nUsage:\n");
  printf("\n./event_dump.exe [-h] [-Hhostname] [-Eexptname] [-p] [-O] [-eMaxEvents] [file1 file2 ...]\n");
  printf("\n");
  printf("\t-h: print this help message\n");
  printf("\t-Hhostname: connect to MIDAS experiment on given host\n");
  printf("\t-Eexptname: connect to this MIDAS experiment\n");
  printf("\t-O: save ODB from midas data file into odbNNNN.xml or .odb file\n");
  printf("\t-e: Number of events to read from input data files\n");
  printf("\t-p: Print bank contents\n");
  printf("\n");
  printf("Example1: print online events: ./event_dump.exe\n");
  printf("Example2: print events from file: ./event_dump.exe /data/alpha/current/run00500.mid.gz\n");
  exit(1);
}

// Main function call

//******************************************************
int main(int argc, char *argv[]) {
//******************************************************
  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
  signal(SIGPIPE, SIG_DFL);

  std::vector<std::string> args;
  for (int i = 0; i < argc; i++) {
    if (strcmp(argv[i],"-h")==0)
      help(); // does not return
      args.push_back(argv[i]);
    }

  const char* hostname = NULL;
  const char* exptname = NULL;
  const char* output_file=NULL;

  // loop over the commandline options
  for (unsigned int i = 1; i < args.size(); i++) {
     const char* arg = args[i].c_str();

     if (strncmp(arg,"-e",2)==0)  // Event cutoff flag (only applicable in offline mode)
  gEventCutoff = atoi(arg+2);
     else if (strncmp(arg,"-H",2)==0)
  hostname = strdup(arg+2);
     else if (strncmp(arg,"-E",2)==0)
  exptname = strdup(arg+2);
  else if (strncmp(arg,"-o",2)==0)
  output_file = strdup(arg+2);
     else if (strncmp(arg,"-O",2)==0)
  gSaveOdb = true;
     else if (strncmp(arg,"-p",2)==0)
  gPrintBank = true;
     else if (strcmp(arg,"-h")==0)
  help(); // does not return
     else if (arg[0] == '-')
  help(); // does not return
   }

  // output initialisation
  // make in the head now


  bool flag = false;

  for (unsigned int i=1; i<args.size(); i++) {
    const char* arg = args[i].c_str();

    if (arg[0] != '-') {
      flag = true;
      ProcessMidasFile(arg, output_file);
    }
  }

  // if we processed some data files,
  // do not go into online mode.
  if (flag)
    return 0;
  return 0;
}

//end
