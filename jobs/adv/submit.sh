#!bin/sh

rm -r temp
mkdir temp

i=0
while IFS=$'\n' read file || [[ -n "$file" ]]; do
  echo "#!/bin/bash" > "temp/${i}.sh"
  echo "" >> "temp/${i}.sh"
  echo "echo \"job ${i}\"" >> "temp/${i}.sh"
  echo "source /cvmfs/sft.cern.ch/lcg/contrib/gcc/4.8/x86_64-centos7-gcc48-opt/setup.sh" >> "temp/${i}.sh"
  echo "source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.14.04/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh" >> "temp/${i}.sh"
  echo "cd /afs/cern.ch/work/s/ssuvorov/dev/TPC_TestBeam/app" >> "temp/${i}.sh"
  echo "${file}" >> "temp/${i}.sh"
  echo "${i}   ${file}"
  i=$((($i+1)))
done < "$1"

condor_submit "Submit.sub"