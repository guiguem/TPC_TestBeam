#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "midas.h"
#include "mygblink.h"
#include <arpa/inet.h>
#include "Event.h"

bool GhostChannel(int channel){

  switch( channel ){
  case 0:
  case 1:
  case 2:
  case 15:
  case 28:
  case 53:
  case 66:
    return true;
    break;
  default:
    return false;
  }
}

int map[100];
int mapC[1728];
int mapR[1728];

void BuildChannelCorrespondance(void){

  static bool FirstEntry = true;

  if( ! FirstEntry ) return;

  int ch = 0;

  for(int i = 0 ; i < 100; i++ ) {
    if( GhostChannel(i) )
      map[i] = -1;
    else
      map[i] = ch++;
  }

  std::fstream MapFile("etc/TPCLPlayout.txt", std::ios::in );

  if (MapFile.fail()){
    std::cout << "Could not open TPCLPlayout.txt "  << std::endl;
    return;
  }

  int Ch = 0;

  do{
    int ASIC;
    int Column,Row;
    int Chan;

    MapFile >> ASIC >> Column >> Row >> Chan ;

    if( !MapFile.fail() ) {
      mapC[Ch] = Column;
      mapR[Ch] = Row;
      Ch++;
    }

  }while(! MapFile.fail());


  std::cout << " Read " << Ch << " channel maps " << std::endl;

  FirstEntry = false;
}


bool Decode(char *buffPtr,int buffSize, Event &event, bool debug=false ) {
  int nByteRead = 0;
  DataPacketHeader *pHeader;
  SubDataPacketHeader *sHeader;
  DataPacketData *pData;
  DataPacketEnd *pEnd;
  unsigned int payload;
  if(debug) std::cout << " Bank SIZE " << buffSize << std::endl;
  int ch = 0;

  BuildChannelCorrespondance();


  //  for( int i = 0 ; i < 10 ; i++ ) {
  //    std::cout << std::hex << (short)buffPtr[i] << std::dec << std::endl;
  //
  //  }

  do {
    // now read data packet header
    pHeader = (DataPacketHeader*) buffPtr;
    buffPtr   += sizeof(DataPacketHeader);
    nByteRead += sizeof(DataPacketHeader);

    payload = ntohs(pHeader->size)- sizeof(DataPacketHeader);
    int femN =  ((ntohs(pHeader->dcchdr))&0x7);
    int dccN =  ((ntohs(pHeader->dcchdr)>>4)&0x3F);
    if( debug ) {
      //      std::cout << std::hex << ntohs(pHeader->dcchdr) << std::dec << std::endl;
      std::cout << " Firmware version " << ((ntohs(pHeader->dcchdr)>>14)&0x3) << std::endl;
      std::cout << " Flag " << ((ntohs(pHeader->dcchdr)>>12)&0x3) << std::endl;
      std::cout << " Reply Type " << ((ntohs(pHeader->dcchdr)>>10)&0x3) << std::endl;
      std::cout << " DCC index " << dccN << std::endl;
      std::cout << " FEM index " << femN  << std::endl;
      std::cout << " >>>>>>>>>>>>>>>>> " << std::endl << " Channel " << ch++ << std::endl;
      std::cout << " Header size " << ntohs(pHeader->size) << std::endl;
      std::cout << " Header hdr " << std::hex << "0x" <<  ntohs(pHeader->hdr) << std::dec << std::endl;
    }
    unsigned int type =  ((ntohs(pHeader->dcchdr)>>10)&0x1);
    unsigned int timeE = ((ntohs(pHeader->ts_h)*1<<16)&0xFFFF0000)+ntohs(pHeader->ts_l)&0xFFFF;
    int tempAsic1 = GET_RB_ARG1(ntohs(pHeader->args));
    int tempAsic2 = GET_RB_ARG2(ntohs(pHeader->args));
    // compute asic#

    if( debug ) {
      std::cout <<" TimeE " <<   timeE << std::endl;
      std::cout << " arg1 " << std::hex <<  tempAsic1  << std::dec <<  std::endl;
      std::cout << " arg2 " << std::hex <<  tempAsic2  << std::dec <<  std::endl;
    }

    int channel,asicN;
    int elchannel = tempAsic1/6;
    asicN = 10*(tempAsic1%6)/2 + tempAsic2;

    channel = map[elchannel]; //remap them to physical channels

    //    if( channel < 0 ) std::cout << " This cannot be " << tempAsic1/6 << std::endl;

    //    std::cout <<  tempAsic1/6 << "  -- > " << channel << std::endl;

    int sampleCount = ntohs(pHeader->scnt); // Total number of samples in Data
    int evNumber= GET_EVENT_COUNT(ntohs(pHeader->ecnt));
    bool compress = GET_RB_COMPRESS((ntohs(pHeader->args)));

    //    if( compress ) {
    //      cm_msg(MINFO,"fetpcdcc","Pedestal data is compressed. Possible error in data format. DCC Message dump: %s",(char*)pHeader);
    //      return false;
    //    }

    if( debug ){
      std::cout << " Event Number " << evNumber << std::endl;
      std::cout << " 1st Sample Count " << sampleCount << std::endl;
      if( !compress )
     	std::cout << " COMPRESS FLG NOT SET IN ASIC " << asicN << " CHANNEL " << channel << std::endl;
    }

    if( type == 0x1 ) { // No DCC data
      buffPtr = buffPtr+payload;
      nByteRead += payload;
      payload = 0;
    }
    else{

      do{
    	// take into account padding short int to get event total
        int sampleCountRead = sampleCount;
        if (sampleCount%2==1)  sampleCountRead++;
	// now read the data
	pData = (DataPacketData*)buffPtr;
	nByteRead+=sampleCountRead*sizeof(unsigned short int);
	payload -= sampleCountRead*sizeof(unsigned short int);
	buffPtr += sizeof(unsigned short int)*sampleCountRead;

	WaveForm wf;

	int channelID = femN*288+asicN*72+channel;// GetchannelCode(dccN,femN,asicN,channel);
	short time = 0;

	wf.compress = compress;
	wf.DCC = dccN;
	wf.FEM     = femN;
	wf.FEC     = asicN/4;
	wf.ASIC    = asicN%4;
	wf.ElCh    = elchannel;
	if( debug ) {
	  std::cout << " DCC " << wf.DCC << " FEM " << wf.FEM << " FEC " << wf.FEC << " ASIC " << wf.ASIC << " Channel " << wf.ElCh << " Phys Channel "
		    << channelID << std::endl;
	}

	wf.Channel = channelID;
	wf.Column = mapC[channelID];
	wf.Row = mapR[channelID];

	//	if( compress ) {
	//	  cm_msg(MINFO,"fetpcdcc","Pedestal data is compressed. Possible error in data format. DCC Message dump: %s",(char*)pHeader);
	// return false;
	//	}

	for (int ip=0; ip<sampleCount; ip++) {
	  unsigned short datum = ntohs(pData->data[ip]);
	  if( datum & CELL_INDEX_FLAG ) { // Compressed date provides the time. We should never enter here.
	    if( !compress ) {
	      cm_msg(MINFO,"fetpcdcc"," Error: time like work (0x1000|val) appears in not compressed flag block (%d)", sampleCount);
	      return false;
	    }
	    time = GET_CELL_INDEX(datum)-10;
	    if( debug )	 std::cout << " Time " << time << std::endl;
	  }
	  else {
	    short adc = (short)datum;
	    wf.ADC.push_back(adc);
	    wf.Time.push_back(time);
	    if( debug ) std::cout << " Time " << time << "  " << adc << std::endl;
	    time++;
	  }
	}  // End of Sample count.

	event.Pulse.push_back(wf);

	if( payload > sizeof(DataPacketEnd) ) { // There is more data in the structure.
	  if (debug )  std::cout << "PAYLOAD " << payload << "   " <<  sizeof(DataPacketEnd)   << std::endl;
	  unsigned short datum = ntohs(*buffPtr);
	  if( (datum&0xF000) == 0x0000 ) {
	    unsigned short datum = ntohs(*buffPtr);
	    buffPtr   += sizeof(short);
	    nByteRead += sizeof(short);
	    payload -= sizeof(short);
	    if( debug ) std::cout << std::hex << " EMPTY SHORT WORD, NEW DATUM = " <<  datum << std::dec << std::endl;
	  }
	  // now read data packet header
	  sHeader = (SubDataPacketHeader*)buffPtr;
	  buffPtr   += sizeof(SubDataPacketHeader);
	  nByteRead += sizeof(SubDataPacketHeader);
	  payload -=  sizeof(SubDataPacketHeader);
	  datum = ntohs(sHeader->args);

	  if( (datum&0xE000) == (unsigned short) 0x2000 ) {
	    int tempAsic1 = GET_RB_ARG1(datum);
	    int tempAsic2 = GET_RB_ARG2(datum);
	    // compute asic#
	    if( debug )
	      std::cout << " arg1 " << std::hex <<  tempAsic1  << std::dec <<  std::endl << " arg2 " << std::hex <<  tempAsic2  << std::dec <<  std::endl;

	    channel = tempAsic1/6;
	    asicN = 10*(tempAsic1%6)/2 + tempAsic2;
	    datum = ntohs(sHeader->scnt);

	    channel = map[channel];

	    if( debug )  std::cout<< " New Channel " << channel << std::endl << std::hex << "Datum 0x"<<  datum << std::dec << std::endl;

	    if(  (datum&0xF000) == 0x4000 ) {
	      sampleCount = datum&0xFFF;
	      if( debug )
		std::cout<< " SampleCount " << sampleCount << std::endl;
	    }
	  }
	}
      } while( payload-sizeof(DataPacketEnd) > 1 );

      pEnd = (DataPacketEnd*)buffPtr;
      buffPtr   += sizeof(DataPacketEnd);
      nByteRead += sizeof(DataPacketEnd);
      payload -= sizeof(DataPacketEnd);

      if( debug ) {
	std::cout << " nBytesRead " << nByteRead << std::endl;
	std::cout << " payload " << payload << std::endl;
      }
    }

  } while( buffSize-nByteRead > 0 );

  if( debug ){
    for(int i = 0; i < buffSize-nByteRead;i++ )
      std::cout << " I ( " << i << " ) = 0x" << std::hex << (short int) *(buffPtr+i*sizeof(short int)) << std::dec << std::endl;

    std::cout << " End of event bank  readout " << std::endl;
  }

  return true;
  //end while nByteRead
}
