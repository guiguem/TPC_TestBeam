#define THIS_NAME ana_adv
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "ilc/common_header.h"
//#include "selection.C"
#include "selection/selection3D.C"
#include "selection/DBSCAN.C"
#include "selection/selection3D_cosmic.C"
#include "selection/selection_ana.C"

//#define COSMIC
//#define PEAK2
//#define TWOSIDE

//#define SEVERE_SUPPRESSION
bool SUPPRESS_ERRORS = true;

void ana_adv() {

#ifdef SEVERE_SUPPRESSION
  gErrorIgnoreLevel = kSysError;
#endif

  bool PLOT = true;

  vector<TString> listOfFiles;
  TString file    = "default.root";
  TString prefix  = "~/";
  for (int iarg=0; iarg<gApplication->Argc(); iarg++){
    if (string( gApplication->Argv(iarg))=="-f" || string( gApplication->Argv(iarg))=="--file" ){
      iarg++;
      file = gApplication->Argv(iarg);
      if (file.Contains(".root")) {

        cout << "adding filename" <<" " << file << endl;
        listOfFiles.push_back(file);
      } else {
        fstream fList(file);
        if (fList.good()) {
          while (fList.good()) {
            string filename;
            getline(fList, filename);
            if (fList.eof()) break;
            listOfFiles.push_back(filename);
          }
        }
      }
    } else if (string( gApplication->Argv(iarg))=="-o" || string( gApplication->Argv(iarg))=="--output" ){
      iarg++;
      prefix = gApplication->Argv(iarg);
    } else if (string( gApplication->Argv(iarg))=="-h" || string( gApplication->Argv(iarg))=="--help" ){
      cout << "**************************************" << endl;
      cout << "Macros run options:" << endl;
      cout << "   -f || --file      input file (*.root) or filelist" << endl;
      cout << "   -o || --output    output folder for PLOTs and out file" << endl;
      cout << "   -h || --help      print help info" << endl;
      cout << "**************************************" << endl;
    }
  }

#if TWOSIDE
#if PEAK2
  cout << "You can not fit 2-side Gaussian and 2 Gaussian peaks at the same moment." << endl;
  cout << "Exit." << endl;
  exit(1);
#endif
#endif

  TCanvas *c1 = new TCanvas("c1","evadc1",0, 0, 400,300);
  TCanvas *c2 = new TCanvas("c2","evadc2",400, 0, 400,300);
  TCanvas *c3 = new TCanvas("c3","evadc3",800, 0, 400,300);
  TCanvas *c4 = new TCanvas("c4","evadc4", 0, 400, 400,300);
  TCanvas *c5 = new TCanvas("c5","evadc5", 400, 400, 400,300);

  //int thr  = 0.;
  double alpha = 0.7;

  TH2F* PadDisplayGeo=new TH2F("PadDisplayGeo","X vs Y of hits",50,-17.15,17.15, 38,-18.13,18.13);
  TH2F* PadDisplayH=new TH2F("PadDisplay1","I vs J of hits",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayH1=new TH2F("PadDisplay2","I vs J of hits",38,-1.,37.,50,-1.,49.);

  TH1F* ClusterCharge     = new TH1F("cluster_charge","Cluster charge",150,0,3000);
  TH1F* ClusterChargeCur  = new TH1F("cluster_chargeC","Cluster charge current",150,0,3000);
  TH1F* ClusterNormCharge = new TH1F("cluster_norm_charge","Truncated mean energy deposit",150,0,1000);

  TH1F* TimeMaximum       = new TH1F("time_max", "time of the maximum", 530, 0., 530.);

  TH2F* PadDisplayHIT     = new TH2F("PadDisplayH","I vs J of hits",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayMAX     = new TH2F("PadDisplayM","I vs J of hits",38,-1.,37.,50,-1.,49.);

  TH1F* PadsPerCluster    = new TH1F("PadsPerCluster", "ppc", 8, 0., 8.);

  TH1F* PadsPerCluster_ar[36];
  for (Int_t i = 0; i < 36; ++i) {
    PadsPerCluster_ar[i]  = new TH1F(Form("PadsPerCluster_%i", i), "ppc", 8, 0., 8.);
  }

  TH1F* XY_angle  = new TH1F("angle_XY", "", 100, 0.8, 1.);
  TH1F* ZY_angle  = new TH1F("angle_ZY", "", 100, 0.8, 1.);
  TH1F* angle     = new TH1F("abs_angle", "", 100, 0.8, 1.);
  TH1F* av_track_t = new TH1F("av_tracktime", "", 511, 0., 511);

  TH1F* angle_bins = new TH1F("angle_bins", "", 10, 0.9, 1.);
  TH1F* ClusterNormCharge_angle[10];
  TH1F* angle_histo = new TH1F("angle_h", "Angle", 204, 0.8, 1.004);
  for (Int_t i = 0; i < 10; ++i)
    ClusterNormCharge_angle[i] = new TH1F(Form("cluster_norm_charge_%i", i),"Truncated mean energy deposit",150,0,1000);

  TH1F* QvsT  = new TH1F("QvsT", "", 520, 0., 520.);
  TH1F* QvsTN = new TH1F("QvsTN", "", 520, 0., 520.);

  TH2F* ChargeCorrelation = new TH2F("corralation", "Charge correlation", 150,0,3000, 150,0,3000);

  vector< vector<int > > EventClusters;

  Int_t total_events  = 0;
  Int_t reco_tracks   = 0;
  Int_t sel_tracks    = 0;

  vector< vector<short> > PadDisplay;
  vector< vector<short> > PadTime;
  vector< vector<short> > PadDisplayNew;
  vector< vector<int> > PadIntegral;
  vector< vector<vector<short> > > PadDisplayV;
  vector< vector< vector<short> > > PadDisplay3D;

  //************************************************************
  //****************** LOOP OVER FILES  ************************
  //************************************************************
  TChain* geo_chain = new TChain("femGeomTree");
  TChain* data      = new TChain("padData");

  for (uint ifile=0;ifile< listOfFiles.size();ifile++){
    file = listOfFiles[ifile];
    data->AddFile(file);
    if (ifile == 0)
      geo_chain->AddFile(file);
  }

  vector<int> *iPad(0);
  vector<int> *jPad(0);
  vector<double> *xPad(0);
  vector<double> *yPad(0);

  vector<double> *dxPad(0);
  vector<double> *dyPad(0);

  geo_chain->SetBranchAddress("jPad", &jPad );
  geo_chain->SetBranchAddress("iPad", &iPad );

  geo_chain->SetBranchAddress("xPad", &xPad );
  geo_chain->SetBranchAddress("yPad", &yPad );

  geo_chain->SetBranchAddress("dxPad", &dxPad );
  geo_chain->SetBranchAddress("dyPad", &dyPad );
  geo_chain->GetEntry(0); // put into memory geometry info
  cout << "Reading channel mapping" << endl;
  cout << "jPad->size()  " << jPad->size() <<endl;
  cout << "iPad->size()  " << iPad->size() <<endl;

  cout << "Reading geometry" << endl;
  cout << "xPad->size()  " << xPad->size() <<endl;
  cout << "yPad->size()  " << yPad->size() <<endl;
  cout << "dxPad->size() " << dxPad->size() <<endl;
  cout << "dyPad->size() " << dyPad->size() <<endl;

  int Imax = -1;
  int Imin = 10000000;
  int Jmax = -1;
  int Jmin = 10000000;

  for (unsigned long i = 0; i < jPad->size(); ++i) {
    if (Imax < (*iPad)[i])
      Imax = (*iPad)[i];
    if (Imin > (*iPad)[i])
      Imin = (*iPad)[i];
    if (Jmax < (*jPad)[i])
      Jmax = (*jPad)[i];
    if (Jmin > (*jPad)[i])
      Jmin = (*jPad)[i];
  }

  gStyle->SetPalette(1);
  gStyle->SetOptStat(0);
  int Nevents=0;

  vector<short>          *listOfChannels(0);
  vector<vector<short> > *listOfSamples(0);

  data->SetBranchAddress("PadphysChannels", &listOfChannels );
  data->SetBranchAddress("PadADCvsTime"   , &listOfSamples );

  if (Nevents <= 0) Nevents = data->GetEntries();
  if (Nevents > data->GetEntries()) Nevents = data->GetEntries();

  cout << "[                                                  ] Nevents = "<<Nevents<<"\r[";

  //************************************************************
  //****************** LOOP OVER EVENTS ************************
  //************************************************************
  for (int ievt=0; ievt < Nevents ; ievt++){
    if (ievt%(Nevents/50)==0)
      cout <<"."<<flush;

    data->GetEntry(ievt);
    PadDisplayH->Reset();
    PadDisplayH1->Reset();

    // clean for the next event
    PadDisplayGeo->Reset();
    PadDisplayNew.clear();
    PadDisplay.clear();
    PadIntegral.clear();
    PadTime.clear();

    PadDisplay.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadDisplay[z].resize(Imax+1, 0);

    PadTime.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadTime[z].resize(Imax+1, 0);

    PadDisplayNew.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadDisplayNew[z].resize(Imax+1, 0);

    PadIntegral.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadIntegral[z].resize(Imax+1, 0);

    PadDisplayV.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadDisplayV[z].resize(Imax+1);

    ClusterChargeCur->Reset();

    if (!(*listOfSamples).size())
      continue;

    int time_bins = (*listOfSamples)[0].size();

    PadDisplay3D.clear();
    PadDisplay3D.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z) {
      PadDisplay3D[z].resize(Imax+1);
      for (int t = 0; t <= Imax; ++t)
        PadDisplay3D[z][t].resize(time_bins, 0);
    }


    //************************************************************
    //*****************LOOP OVER CHANNELS ************************
    //************************************************************
    for (uint ic=0; ic< listOfChannels->size(); ic++){
      int chan= (*listOfChannels)[ic];
      // find out the maximum
      float adcmax=-1;
      Int_t it_max = -1;

      // one maximum per channel
      for (uint it = TIME_FIRST; it <= TIME_LAST; it++){
        int adc= (*listOfSamples)[ic][it];
        PadDisplay3D[(*jPad)[chan]][(*iPad)[chan]][it] = adc;
        if (adc>adcmax && adc > 0) {
          adcmax = adc;
          it_max = it;
        }
      }

      // noise suppression
      if (it_max < TIME_FIRST_REL || it_max > TIME_LAST_REL)
        continue;

      int lower = max (it_max - 10, 5);
      int upper = min(it_max + 20, 490);
      int integral = 0;
      for (int it = lower; it < upper; it++){
        // stop integrating if noise
        if ((*listOfSamples)[ic][it] < 0) {
          if (it > it_max)
            break;
          else continue;
        }

        integral += (*listOfSamples)[ic][it];
      }

      if (adcmax<0) continue; // remove noise
      //if ((*iPad)[chan] == Imin || (*iPad)[chan] == Imax ||
      //    (*jPad)[chan] == Jmin || (*jPad)[chan] == Jmax)
      //    continue;

      PadDisplay[(*jPad)[chan]][(*iPad)[chan]]  = adcmax;
      PadTime[(*jPad)[chan]][(*iPad)[chan]]     = it_max;
      PadIntegral[(*jPad)[chan]][(*iPad)[chan]] = integral;

    } //loop over channels

    ++total_events;

#ifndef SEVERE_SUPPRESSION
    Int_t gErrorIgnoreLevel_bu = kSysError;
    if (SUPPRESS_ERRORS) {
      gErrorIgnoreLevel_bu = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kSysError;
    }
#endif
    // apply the selection
#ifdef COSMIC
    vector<vector< vector<short> > > MultiOutPad;
    vector<vector< vector<short> > > MultiOutPadTime;
    if (!DBSCANSelectionCosmic(PadDisplay, PadTime, PadDisplayNew, MultiOutPad, MultiOutPadTime))
    //if (!SelectionTrackCosmic(PadDisplay, PadTime, PadDisplayNew))
        continue;
#else
    vector<vector< vector<short> > > MultiOutPad;
    vector<vector< vector<short> > > MultiOutPadTime;
    //if (!TestSelection(PadDisplay, PadTime, PadDisplayNew, MultiOutPad))
    if (!TestSelection3D(PadDisplay, PadTime, PadDisplayNew, MultiOutPad, MultiOutPadTime, PadDisplay3D))
    //if (!DBSCANSelection(PadDisplay, PadTime, PadDisplayNew, MultiOutPad))
      continue;
#endif

#ifndef SEVERE_SUPPRESSION
    if (SUPPRESS_ERRORS)
      gErrorIgnoreLevel = gErrorIgnoreLevel_bu;
#endif

    for (uint trackId = 0; trackId < MultiOutPad.size(); ++trackId) {
      ++reco_tracks;

      //if (!CosmicLeft(MultiOutPad[trackId], 25.))
        //continue;
      // 1 - 70, 160
      // 2 - 120, 190
      // 3 - 220, 290
      // 10, 11 - 150, 230
      if (!TimeCut(MultiOutPad[trackId], MultiOutPadTime[trackId], 120., 190.))
        continue;
      TVector3 start, end;
      //GetTrackPos_3D(MultiOutPad[trackId], MultiOutPadTime[trackId], start, end, true);

      ++sel_tracks;

      //cout << "S1:\t" << start.X() << "\t" << start.Y() << "\t" << start.Z() << endl;
      //cout << "E1:\t" << end.X() << "\t" << end.Y() << "\t" << end.Z() << endl;


      start.SetZ(start.Z()*0.08*5.3);
      end.SetZ(end.Z()*0.08*5.3);

      start.SetX(start.X()*0.7);
      end.SetX(end.X()*0.7);

      start.SetY(start.Y()*0.9);
      end.SetY(end.Y()*0.9);

      //cout << "S2:\t" << start.X() << "\t" << start.Y() << "\t" << start.Z() << endl;
      //cout << "E2:\t" << end.X() << "\t" << end.Y() << "\t" << end.Z() << endl;

      TVector3* start_XY = (TVector3*)start.Clone(); start_XY->SetZ(0.);
      TVector3* end_XY = (TVector3*)end.Clone(); end_XY->SetZ(0.);

      TVector3* start_ZY = (TVector3*)start.Clone(); start_ZY->SetY(0.);
      TVector3* end_ZY = (TVector3*)end.Clone(); end_ZY->SetY(0.);

      XY_angle->Fill(abs((*end_XY - *start_XY).Unit().X()));
      ZY_angle->Fill(abs((*end_ZY - *start_ZY).Unit().X()));
      angle->Fill(abs((end-start).Unit().X()));

      //cout << (*end_XY - *start_XY).Unit().X() << "   " << (*end_ZY - *start_ZY).Unit().X() << "     " << (end-start).Unit().X() << endl;

      av_track_t->Fill(GetAverageTime(MultiOutPad[trackId], MultiOutPadTime[trackId]));

      for (int j = 0; j < 48; ++j) {
        for (int i = 0; i < 36; ++i) {
          if (!PadDisplayNew[j][i])
            continue;
          TimeMaximum->Fill(PadTime[j][i]);
        }
      }

      // charge per cluster study
      vector<int > cluster_charge;
      cluster_charge.clear();
      int offset = 0;

#ifdef COSMIC
      float cos_theta = GetCosTheta(MultiOutPad[trackId], true);
      for (Int_t it_j = offset; it_j < Jmax - offset; ++it_j) {
#else
        float cos_theta = GetCosTheta(MultiOutPad[trackId], false);
      for (Int_t it_i = offset; it_i < Imax - offset; ++it_i) {
#endif
        Int_t cluster = 0;
        Int_t pads_per_cluster = 0;

#ifdef COSMIC
        for (Int_t it_i = 0; it_i < Imax; ++it_i) {
#else
        for (Int_t it_j = 0; it_j < Jmax; ++it_j) {
#endif
          cluster += MultiOutPad[trackId][it_j][it_i];
          if (MultiOutPad[trackId][it_j][it_i] > 0) {
            ++pads_per_cluster;
            PadDisplayHIT->Fill(it_i, it_j, 1);
          }
          PadDisplayMAX->Fill(it_i, it_j, MultiOutPad[trackId][it_j][it_i]);
          int integral = PadIntegral[it_j][it_i];
          QvsT->Fill(PadTime[it_j][it_i], integral);
          QvsTN->Fill(PadTime[it_j][it_i]);
        }

        if (cluster != 0) {
          PadsPerCluster->Fill(pads_per_cluster);
#ifndef COSMIC
          PadsPerCluster_ar[it_i]->Fill(pads_per_cluster);
#endif
          ClusterCharge->Fill(cluster);
          ClusterChargeCur->Fill(cluster);
          cluster_charge.push_back(cluster);

          UInt_t clusterID = cluster_charge.size();
          if (clusterID != 1)
            ChargeCorrelation->Fill(cluster_charge[clusterID-1], cluster_charge[clusterID-2]);
        }
      }

      sort(cluster_charge.begin(), cluster_charge.end());
      EventClusters.push_back(cluster_charge);
      Float_t norm_cluster = 0.;
      Int_t i_max = round(alpha * cluster_charge.size());
      for (int i = 0; i < std::min(i_max, int(cluster_charge.size())); ++i)
        norm_cluster += cluster_charge[i];

      norm_cluster *= 1 / (alpha * cluster_charge.size());

      ClusterNormCharge->Fill(norm_cluster);
      //cout << "cos theta " << cos_theta << endl;
      angle_histo->Fill(abs(cos_theta));
      if (abs(cos_theta) >= 0.8) {
        int bin = angle_bins->GetXaxis()->FindBin(abs(cos_theta)) - 1;
        if (bin == 10)
          bin = 9;
        ClusterNormCharge_angle[bin]->Fill(norm_cluster);
      }

      if (PLOT) {
        cout << "Evt " << ievt << endl;
        TH2F* padHisto1 = new TH2F("pad1", "Raw event", 36, 0., 36., 48, 0., 48.);
        TH2F* padHisto2 = new TH2F("pad2", "Selected track", 36, 0., 36., 48, 0., 48.);
        TH2F* padHisto3 = new TH2F("pad2", "Raw event", 36, 0., 36., 48, 0., 48.);

        for (int j = 0; j < 48; ++j) {
          for (int i = 0; i < 36; ++i) {
            padHisto1->SetBinContent(i, j, PadDisplay[j][i]);
            padHisto2->SetBinContent(i, j, MultiOutPad[trackId][j][i]);
            padHisto3->SetBinContent(i, j, PadTime[j][i]);
          }
        }

        c1->cd();
        padHisto1->Draw("colz");
        gPad->Update();
        c1->Modified();
        c1->Update();

        c2->cd();
        padHisto2->Draw("colz");
        c2->Modified();
        c2->Update();

        c3->cd();
        padHisto3->Draw("colz");
        gPad->Update();
        c3->WaitPrimitive();
      }
    } // loop over selected tracks
  } // loop over events
  cout << "]" << endl;

  cout << "Total events number     : " << total_events << endl;
  cout << "Reco tracks             : " << reco_tracks << endl;
  cout << "Selected tracks         : " << sel_tracks << endl;

  gStyle->SetOptStat("RMne");
  TFile* out_file = new TFile((prefix + "output_adv.root").Data(), "RECREATE");

  c1->cd();

  ClusterCharge->Draw();
  ClusterCharge->Write();
  c1->Print((prefix+"ClusterCharge_adv.pdf").Data());

#ifdef PEAK2
  TF1* f1 = new TF1("f1", "[0] * TMath::Gaus(x, [1], [2]) + [3] * TMath::Gaus(x, [4], [5])", 0, 1300);
#elif TWOSIDE
  TF1* f1 = new TF1("f1", "( (x < [1] ) ? [0] * TMath::Gaus(x, [1], [2]) : [0] * TMath::Gaus(x, [1], [3]))", 0, 1300);
#else
  TF1* f1 = new TF1("f1", "([0] * TMath::Gaus(x, [1], [2]))", 0, 1300);
#endif

#ifdef COSMIC
  f1->SetParameters(2500, 200, 50, 50);
#else
#ifdef PEAK2
  f1->SetParameters(200, 300, 50, 300, 400, 50);
#elif TWOSIDE
  f1->SetParameters(200, 700, 50, 50);
#else
  f1->SetParameters(900, 500, 30);
#endif
#endif
  f1->SetParName(0, "Const");

#ifdef PEAK2
  f1->SetParName(0, "Const1");
  f1->SetParName(1, "Mean1");
  f1->SetParName(2, "Sigma1");
  f1->SetParName(3, "Const2");
  f1->SetParName(4, "Mean2");
  f1->SetParName(5, "Sigma2");
#elif TWOSIDE
  f1->SetParName(1, "Mean");
  f1->SetParName(2, "Left sigma");
  f1->SetParName(3, "Right sigma");
#else
  f1->SetParName(1, "Mean");
  f1->SetParName(2, "Sigma");
#endif

  c2->cd();
  TH1F* ClusterNormChargeNF = (TH1F*)ClusterNormCharge->Clone("norm_charge_no_fit");
  ClusterNormChargeNF->Write();
  ClusterNormCharge->Fit("gaus", "", "", 0, 500);

  gStyle->SetOptFit(1);

  ClusterNormCharge->Draw();
  gPad->Update();
  ClusterNormCharge->Write();
  c2->Print((prefix+"ClusterChargeNorm_adv.pdf").Data());

  gStyle->SetOptFit(0);

  c3->cd();
  TimeMaximum->Draw();
  TimeMaximum->Write();
  c3->Print((prefix+"TimeMaximum_adv.pdf").Data());
  c4->cd();

  gStyle->SetOptStat(0);

  c4->cd();
  PadDisplayHIT->Draw("colz");
  PadDisplayHIT->Write();
  c4->Print((prefix+"pad_scan_hit_track_adv.pdf").Data());
  PadDisplayMAX->Draw("colz");
  PadDisplayMAX->Write();
  c4->Print((prefix+"pad_scan_max_track_adv.pdf").Data());


  PadsPerCluster->Draw();
  PadsPerCluster->Write();
  c4->Print((prefix+"PadsPerCluster_adv.pdf").Data());

  TH2F* PadDisplayMAX2 = (TH2F*)PadDisplayMAX->Clone("avg_charge");
  PadDisplayMAX2->Divide(PadDisplayHIT);
  PadDisplayMAX2->Draw("colz");
  PadDisplayMAX2->Write();
  c4->Print((prefix+"AvChargePerHit_adv.pdf").Data());

  c5->cd();
  QvsT->Divide(QvsTN);
  QvsT->Write();

  ChargeCorrelation->Draw("colz");
  ChargeCorrelation->Write();

  XY_angle->Write();
  ZY_angle->Write();
  angle->Write();
  av_track_t->Write();

  for (Int_t i = 0; i < 10; ++i) {
    ClusterNormCharge_angle[i]->Write();
  }
  angle_histo->Write();
#ifndef COSMIC
  for (Int_t i = 0; i < 36; ++i) {
    PadsPerCluster_ar[i]->Write();
  }
#endif

  // loop over alpha vars per optimization
  // loop over alpha
  Float_t min   = 0.3;
  Float_t max   = 0.9;
  Int_t   Nstep = 24;

  TGraphErrors* gr = new  TGraphErrors();

  Float_t step = (max- min) / Nstep;

  //************************************************************
  //************************************************************
  //*****************LOOP OVER ALPHA ;;;************************
  //************************************************************
  //************************************************************
  for (Int_t z = 0; z <= Nstep; ++z) {
    Float_t alpha = min + z * step;
    TH1F* ClusterNormCharge = new TH1F("cluster_norm_charge","Truncated mean energy deposit",2500,0,2500);

    for (UInt_t j = 0; j < EventClusters.size(); ++j) {
      Float_t norm_cluster = 0.;
      Int_t i_max = round(alpha * EventClusters[j].size());
      Int_t v_size = (i_max < int(EventClusters[j].size())) ? i_max : int(EventClusters[j].size());
      for (int i = 0; i < v_size ; ++i)
        norm_cluster += EventClusters[j][i];

      norm_cluster *= 1 / (alpha * EventClusters[j].size());

      ClusterNormCharge->Fill(norm_cluster);
    }
    gStyle->SetOptStat("RMne");
    gStyle->SetOptFit(0111);
    ClusterNormCharge->Fit("gaus", "Q");//, "", 420., 650.);
    TF1 *fit = ClusterNormCharge->GetFunction("gaus");
    //Float_t constant  = fit->GetParameter(0);
    Float_t mean      = fit->GetParameter(1);
    Float_t sigma     = fit->GetParameter(2);

    Float_t mean_e     = fit->GetParError(1);
    Float_t sigma_e    = fit->GetParError(2);
    Float_t resol = sigma / mean;

    Float_t resol_e   = resol * sqrt(mean_e*mean_e/(mean*mean) + sigma_e*sigma_e/(sigma*sigma));
    //****************************************************************************
    //int bin1 = ClusterNormCharge->FindFirstBinAbove(ClusterNormCharge->GetMaximum()/2);
    //int bin2 = ClusterNormCharge->FindLastBinAbove(ClusterNormCharge->GetMaximum()/2);
    //double fwhm = ClusterNormCharge->GetBinCenter(bin2) - ClusterNormCharge->GetBinCenter(bin1);
    //resol = 0.5 * fwhm / ClusterNormCharge->GetBinCenter(ClusterNormCharge->GetMaximumBin());

    gr->SetPoint(z, alpha, resol*100.);

    gr->SetPointError(z, 0, resol_e*100.);
  }

  gr->SetMarkerColor(4);
  gr->SetMarkerSize(0.5);
  gr->SetMarkerStyle(21);
  gr->SetName("alpha");
  gr->Write();


  out_file->Close();

  exit(1);

  return;
}
