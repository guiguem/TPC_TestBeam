#include <iostream>
#include <vector>
#include "../ilc/common_header.h"

using namespace std;
//////---------- COSMIC -----------///////


//*******************************************************
bool DBSCANSelectionCosmic(vector< vector<short> > PadDisplay, vector< vector<short> > PadTime, vector< vector<short> >& OutPad, vector<vector< vector<short> > >& MultiOutPad, vector< vector< vector<short> > >& MultiOutPadTime) {
//*******************************************************

  int sel_pads = -1;
  int distance = -1;
  int pads_ana = -1;
  int t1= -1;
  int t2= -1;
  int indexMax = -1;
  int ChargeCut = 0;
  int minTime = 10;
  int maxTime = 490;
  int distMin = 5;
  int numPadsCut = 1;
  double fact = 12.5;

  bool allowMULTITRACK = true;

 // TH2F* h_sel_clusters[256];
  vector<int> Npads;

  vector< vector <short> > outPadTime;
  outPadTime.resize(OutPad.size());
  for (uint i = 0; i < outPadTime.size(); ++i)
    outPadTime[i].resize(OutPad[i].size(), 0);

  Npads.clear();

  //in the coments of this selection "pad" should be understood as a "3D pad", with coordinates (j,i,PadTime[j][i])
  //reminder: PadTime[j][i] contains the time asociated to the maxADC, this can introduce noise in case of track overlapping.
  //accordingly a more delicate treatment of time should be done in the future.

  // First we clean the input sample of pads with charge below ChargeCut or produce before minTime or after maxTime;

 if(DEBUG) cout << endl << "NEW EVENT" << endl << endl;

  for (int j = 0; j < 48; j++){
      for (int i = 0; i < 36; ++i) {
        if (PadDisplay[j][i] <= ChargeCut) PadDisplay[j][i] = 0;
        if (PadTime[j][i] < minTime || PadTime[j][i] > maxTime) PadDisplay[j][i] = 0;
      }
  }

  vector< vector<int> > TotalSelectedPads;
  vector< vector<int> > TmpSelectedPads;
  vector< vector<int> > AlreadyAnalyzed;
  vector< vector<int> >  NewClusterSelectedPads;
  vector< vector<vector<int> > > FinalSelCluster;

  TotalSelectedPads.clear();
  TmpSelectedPads.clear();
  AlreadyAnalyzed.clear();
  NewClusterSelectedPads.clear();
  FinalSelCluster.clear();

  TotalSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TotalSelectedPads[z].resize(36);}
  TmpSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TmpSelectedPads[z].resize(36);}
  AlreadyAnalyzed.resize(48);
  for (int z=0; z < 48; ++z) {AlreadyAnalyzed[z].resize(36);}
  NewClusterSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {NewClusterSelectedPads[z].resize(36);}

  FinalSelCluster.resize(48);
  for (int z=0; z < 48; ++z) FinalSelCluster[z].resize(36);

  //The main loop of the DBSCAN iterates over all pads in the cleaned input sample to cluster them.

  for (int j = 0; j < 48; j++){
    for (int i = 0; i < 36; ++i) {

      if (PadDisplay[j][i] <= 0)  continue;
      if (TotalSelectedPads[j][i]) continue; // avoids clustering if the pad has been already included in a cluster

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }

      sel_pads = 0;
      if(sel_pads == 0) {NewClusterSelectedPads[j][i] = PadDisplay[j][i]; sel_pads = 1;}// starting pad for the new cluster

      pads_ana=0;

      while (pads_ana < sel_pads){
        for (int l = 0; l < 48; l++) {
          for (int k = 0; k < 36; ++k) {

            if(!NewClusterSelectedPads[l][k]) continue; // only analyse pads in the new cluster
            if(AlreadyAnalyzed[l][k]) continue;  // ignore pads already considered for the new cluster

            pads_ana++;

            if(DEBUG) cout << endl << "analysing: " << l << " | " << k << endl << endl;

            AlreadyAnalyzed[l][k] = 1;
            t1 = PadTime[l][k];
            int cnt=0;

            for (int n = 0; n < 48; n++) {
              for (int m = 0; m < 36; ++m) {
                if (PadDisplay[n][m] <= 0)  continue;
                if (m == k && n ==l) continue;
                  t2 = PadTime[n][m];
                  distance =  pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5);
                if (distance < distMin) {
                  cnt++;
                  TmpSelectedPads[n][m]=1;
                  if(DEBUG) cout << n << " | " << m << "| dist: " << distance <<  endl;
                } // count how many pads are inside the 3D bubble
              }
            }

            if(DEBUG) cout << "Added to selection: " << cnt << endl;
            if(cnt>=numPadsCut){
              if(DEBUG) cout << "Clustered?: Yes!" << endl;
              sel_pads+=cnt;
              for (int q = 0; q < 48; q++) {
                for (int p = 0; p < 36; ++p) {
                  if(!TmpSelectedPads[q][p]) continue;
                  NewClusterSelectedPads[q][p] = 1;
                }
              }
            }
            if (DEBUG && cnt<numPadsCut) cout << "Clustered?: No!" << endl;

            sel_pads=0;
            for (int q = 0; q < 48; q++) {
              for (int p = 0; p < 36; ++p) {
                if(!NewClusterSelectedPads[q][p]) continue;
                sel_pads++;
              }
            }

            if(DEBUG) cout << "selected: " << sel_pads << endl;
            if(DEBUG) cout << "analysed: " << pads_ana << endl;
          }
        }
      }

      Npads.push_back(sel_pads);

      for (int s = 0; s < 48; s++) {
        for (int r = 0; r < 36; ++r) {
          if(!NewClusterSelectedPads[s][r]) {FinalSelCluster[s][r].push_back(-1); continue;}
          TotalSelectedPads[s][r] = 1;
          FinalSelCluster[s][r].push_back(PadDisplay[s][r]);
        }
      }

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }
    }
  }

  if (!Npads.size()) return false;

  if(!allowMULTITRACK){
    indexMax = 0;
    int maxNPads = -1;
    for(int g=0; g< (int) Npads.size(); g++){
      if(Npads[g] > maxNPads) {maxNPads = Npads[g]; indexMax = g;}
    }
  }
  else indexMax = -1;

  int selec = 0;
  for (int x=0; x< (int) Npads.size(); x++){

    if(allowMULTITRACK == false && x != indexMax) continue; // Disable multitrack, only take the track with more hits!

    selec=0;
    uint index = x;

    if (Npads[index] < 50 || Npads[index] > 150) continue;
    if(DEBUG) cout << "trackPads: " << Npads[index]  << endl;

    for (int jj = 0; jj < 48; jj++){
      for (int ii = 0; ii < 36; ++ii) {
        if(FinalSelCluster[jj][ii][index]>0){
          OutPad[jj][ii] = PadDisplay[jj][ii];
          outPadTime[jj][ii] = PadTime[jj][ii];
        }
      }
    }

int n_pads = 0;
TH2F* padHisto = new TH2F("padhisto", "", 48, 0., 48., 36, 0., 36.);
  for (int j = 0; j < 36; ++j) {
    for (int i = 0; i < 48; ++i) {
      if (OutPad[i][j]) {
        padHisto->SetBinContent(i+1, j+1, PadDisplay[i][j]);
        n_pads++;
      }
    }
  }

    int done = 0;
    (void)done;
    int n_col = 0;
    for (int i = 0; i < 48; ++i) {
      done = 0;
      for (int j = 0; j < 36; ++j) {
        if (OutPad[i][j]) {
          n_col++;
        }
      }
      if (n_col > 7) selec++;
      n_col = 0;
    }

    padHisto->Fit("pol1", "Q");
    TF1* fit = padHisto->GetFunction("pol1");

    if (!fit){
      delete padHisto;
      return false;
    }

    double quality = fit->GetChisquare() / fit->GetNDF();
    double k = fit->GetParameter(1);
    double b = fit->GetParameter(0);

    delete padHisto;

    if (DEBUG) cout << "Fitted with a line k = " << k << " b = " << b << " quality = " << quality << endl;

    if(abs(k)>0.1 || quality > 200)  selec++;

    if(!selec) {
      MultiOutPad.push_back(OutPad);
      MultiOutPadTime.push_back(outPadTime);
    }

    for (int jj = 0; jj < 48; jj++){
      for (int ii = 0; ii < 36; ++ii){
        OutPad[jj][ii] = 0;
        outPadTime[jj][ii] = 0;
      }
    }

  }

  return true;
}


//*******************************************************
bool TimeFilterCosmic(vector< vector<short> > &PadSelected, vector< vector<short> > &PadTime, vector< vector<short> > PadDisplay, vector< vector<vector<short> > > PadDisplayV) {
//*******************************************************

  int sel_pads = -1;
  int distance = -1;
  int pads_ana = -1;
  int t1= -1;
  int t2= -1;
  int distMin = 5;
  int numPadsCut = 1;
  double fact = 12.5;
  int addittions = 0;

 // TH2F* h_sel_clusters[256];
  vector<int> Npads;

  Npads.clear();

  //in the coments of this selection "pad" should be understood as a "3D pad", with coordinates (j,i,PadTime[j][i])
  //reminder: PadTime[j][i] contains the time asociated to the maxADC, this can introduce noise in case of track overlapping.
  //accordingly a more delicate treatment of time should be done in the future.

  // First we clean the input sample of pads with charge below ChargeCut or produce before minTime or after maxTime;

 if(DEBUG) cout << endl << "NEW EVENT" << endl << endl;

  vector< vector<int> > TotalSelectedPads;
  vector< vector<int> > TmpSelectedPads;
  vector< vector<int> > AlreadyAnalyzed;
  vector< vector<int> >  NewClusterSelectedPads;
  vector< vector<vector<int> > > FinalSelCluster;

  TotalSelectedPads.clear();
  TmpSelectedPads.clear();
  AlreadyAnalyzed.clear();
  NewClusterSelectedPads.clear();
  FinalSelCluster.clear();

  TotalSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TotalSelectedPads[z].resize(36);}
  TmpSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TmpSelectedPads[z].resize(36);}
  AlreadyAnalyzed.resize(48);
  for (int z=0; z < 48; ++z) {AlreadyAnalyzed[z].resize(36);}
  NewClusterSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {NewClusterSelectedPads[z].resize(36);}

  FinalSelCluster.resize(48);
  for (int z=0; z < 48; ++z) FinalSelCluster[z].resize(36);

  //The main loop of the DBSCAN iterates over all pads in the cleaned input sample to cluster them.

  for (int j = 0; j < 48; j++){
    for (int i = 0; i < 36; ++i) {

      if (PadSelected[j][i] <= 0)  continue;
      if (TotalSelectedPads[j][i]) continue; // avoids clustering if the pad has been already included in a cluster

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }

      sel_pads = 0;
      if(sel_pads == 0) {NewClusterSelectedPads[j][i] = PadSelected[j][i]; sel_pads = 1;}// starting pad for the new cluster

      pads_ana=0;

      while (pads_ana < sel_pads){
        for (int l = 0; l < 48; l++) {
          for (int k = 0; k < 36; ++k) {

            if(!NewClusterSelectedPads[l][k]) continue; // only analyse pads in the new cluster
            if(AlreadyAnalyzed[l][k]) continue;  // ignore pads already considered for the new cluster

            pads_ana++;

            if(DEBUG) cout << endl << "analysing: " << l << " | " << k << endl << endl;

            AlreadyAnalyzed[l][k] = 1;
            t1 = PadTime[l][k];
            int cnt=0;

            for (int n = 0; n < 48; n++) {
              for (int m = 0; m < 36; ++m) {
                if (PadDisplay[n][m] <= 0)  continue;
                if (m == k && n ==l) continue;
                  t2 = PadTime[n][m];
                  if(PadSelected[n][m] > 0 ) distance =  pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5);
                  else{
                    if (DEBUG) cout << "dist:" << pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5) << endl;
                    if (DEBUG) cout << "time diff: " << abs(t1-t2) << endl;
                    if(pow(pow(abs(m-k),2)+pow(abs(n-l),2),0.5) > 2.01) continue;
                    int tWindow = 40;
                    if(abs(t1-t2)>60){
                      int maxt2 = 0;
                      int it_t2 = 0;
                      for (int qq=t1-tWindow/2; qq<=t1+tWindow/2; qq++){
                        if(!(t2-tWindow/2 > 10  && t2+tWindow/2 < 490 )) continue;
                        if(PadDisplayV[n][m][qq] > maxt2) {maxt2 = PadDisplayV[n][m][qq]; it_t2 = qq;}
                      }
                      if (it_t2 != 0){
                        if (DEBUG)  cout << "t2 change from " << t2 << " to: " << it_t2 << endl;
                        if (DEBUG)  cout << "Q change from " << PadDisplayV[n][m][t2] << " to: " << PadDisplayV[n][m][it_t2] << endl;
                        PadDisplay[n][m] = PadDisplayV[n][m][it_t2];
                        PadTime[n][m] = it_t2;
                        t2 = it_t2;
                        addittions++;
                      }
                      distance =  pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5);
                    if (DEBUG)  if(distance < distMin) cout << "MODIFIED in Filter at:  i = " << m << " j = " << n << endl;
                    }
                  }
                if (distance < distMin) {
                  cnt++;
                  TmpSelectedPads[n][m]=1;
                  if(DEBUG) cout << n << " | " << m << "| dist: " << distance <<  endl;
                } // count how many pads are inside the 3D bubble
              }
            }

            if(DEBUG) cout << "Added to selection: " << cnt << endl;
            if(cnt>=numPadsCut){
              if(DEBUG) cout << "Clustered?: Yes!" << endl;
              sel_pads+=cnt;
              for (int q = 0; q < 48; q++) {
                for (int p = 0; p < 36; ++p) {
                  if(!TmpSelectedPads[q][p]) continue;
                  NewClusterSelectedPads[q][p] = 1;
                }
              }
            }
            if (DEBUG && cnt<numPadsCut) cout << "Clustered?: No!" << endl;

            sel_pads=0;
            for (int q = 0; q < 48; q++) {
              for (int p = 0; p < 36; ++p) {
                if(!NewClusterSelectedPads[q][p]) continue;
                sel_pads++;
              }
            }

            if(DEBUG) cout << "selected: " << sel_pads << endl;
            if(DEBUG) cout << "analysed: " << pads_ana << endl;
          }
        }
      }

      Npads.push_back(sel_pads);

      for (int s = 0; s < 48; s++) {
        for (int r = 0; r < 36; ++r) {
          if(!NewClusterSelectedPads[s][r]) {FinalSelCluster[s][r].push_back(-1); continue;}
          TotalSelectedPads[s][r] = 1;
          FinalSelCluster[s][r].push_back(PadDisplay[s][r]);
        }
      }

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }
    }
  }


  uint index = 0;
  uint selec = 0;

  if (!Npads[index]) return false;

  if (Npads[index] < 50 || Npads[index] > 150) return false;
  if(DEBUG) cout << "trackPads: " << Npads[index]  << endl;

  for (int jj = 0; jj < 48; jj++){
    for (int ii = 0; ii < 36; ++ii) {
      if(FinalSelCluster[jj][ii][index]>0){
        PadSelected[jj][ii] = PadDisplay[jj][ii];
      }
    }
  }

int n_pads = 0;
TH2F* padHisto = new TH2F("padhisto", "", 48, 0., 48., 36, 0., 36.);
  for (int j = 0; j < 36; ++j) {
    for (int i = 0; i < 48; ++i) {
      if (PadSelected[i][j]) {
        padHisto->SetBinContent(i+1, j+1, PadDisplay[i][j]);
        n_pads++;
      }
    }
  }

  int potDanger = -1;
  int dLast = -99;
  int dNext = -99;
  int nholes = 0;

  int done = 0;
  int n_col = 0;
//  int t_cnt = 0;
  for (int i = 0; i < 48; ++i) {
    done = 0;
    potDanger = -1;
    for (int j = 0; j < 36; ++j) {
      if (PadSelected[i][j]) {
        n_col++;
        dLast = -99;
        dNext = -99;
        if (j!=0) {if (PadSelected[i][j]>0 && PadSelected[i][j-1] ==0) dLast = -1;}
        if (j!=35) {if (PadSelected[i][j]>0 && PadSelected[i][j+1] ==0) dNext = 1;}
        if(dLast==-1 && dNext==1) potDanger = j;
//        if (PadTime[i][j] > 470 || PadTime[i][j] < 20) t_cnt++;
      }
      if(n_col>0 && !PadSelected[i][j]) done = 1;
      if(done == 1 && PadSelected[i][j]>0 && potDanger == -1) selec++;
      if(done == 1 && PadSelected[i][j]>0 && potDanger >= 0 && potDanger < 48 && n_col > 2) PadSelected[i][potDanger]=0;
      if(done == 1 && PadSelected[i][j]>0 && potDanger >= 0 && potDanger < 48 && n_col <= 2) selec++;

      if(j == 35 && n_col == 0) nholes++;
    }
    if (n_col > 7) selec++;
    n_col = 0;
  }

//  if (nholes >= 3) selec++;
//  if(t_cnt > 15) selec++;

  padHisto->Fit("pol1", "Q");
  TF1* fit = padHisto->GetFunction("pol1");

  if (!fit){
    delete padHisto;
    return false;
  }

  double quality = fit->GetChisquare() / fit->GetNDF();
  double k = fit->GetParameter(1);
  double b = fit->GetParameter(0);

  delete padHisto;

  if (DEBUG) cout << "Fitted with a line k = " << k << " b = " << b << " quality = " << quality << endl;

  if(abs(k)>0.1 || quality > 200)  selec++;

  if(selec > 0) return false;
  if(addittions > 0) return false;

  if (DEBUG) cout << "addittions: " << addittions << endl;
  if (DEBUG) cout << "Filter Success!" << endl;
  return true;
}





bool DBSCANSelectionCosmic(vector< vector<short> > PadDisplay, vector< vector<short> > PadTime, vector< vector<short> >& OutPad, vector<vector< vector<short> > >& MultiOutPad) {
  vector<vector< vector<short> > > MultiOutPadTime;
  return DBSCANSelectionCosmic(PadDisplay, PadTime, OutPad, MultiOutPad, MultiOutPadTime);
}














//////---------- BEAM -----------///////



//*******************************************************
bool DBSCANSelectionBeam(vector< vector<short> > PadDisplay, vector< vector<short> > PadTime, vector< vector<short> >& OutPad, vector<vector< vector<short> > >& MultiOutPad) {
//*******************************************************

  int sel_pads = -1;
  int distance = -1;
  int pads_ana = -1;
  int t1= -1;
  int t2= -1;
  int indexMax = -1;
  int ChargeCut = 0;
  int minTime = 10;
  int maxTime = 490;
  int distMin = 5;
  int numPadsCut = 1;
  double fact = 12.5;

  bool allowMULTITRACK = true;

 // TH2F* h_sel_clusters[256];
  vector<int> Npads;

  Npads.clear();

  //in the coments of this selection "pad" should be understood as a "3D pad", with coordinates (j,i,PadTime[j][i])
  //reminder: PadTime[j][i] contains the time asociated to the maxADC, this can introduce noise in case of track overlapping.
  //accordingly a more delicate treatment of time should be done in the future.

  // First we clean the input sample of pads with charge below ChargeCut or produce before minTime or after maxTime;

 if(DEBUG) cout << endl << "NEW EVENT" << endl << endl;

  for (int j = 0; j < 48; j++){
      for (int i = 0; i < 36; ++i) {
        if (PadDisplay[j][i] <= ChargeCut) PadDisplay[j][i] = 0;
        if (PadTime[j][i] < minTime || PadTime[j][i] > maxTime) PadDisplay[j][i] = 0;
      }
  }

  vector< vector<int> > TotalSelectedPads;
  vector< vector<int> > TmpSelectedPads;
  vector< vector<int> > AlreadyAnalyzed;
  vector< vector<int> >  NewClusterSelectedPads;
  vector< vector<vector<int> > > FinalSelCluster;

  TotalSelectedPads.clear();
  TmpSelectedPads.clear();
  AlreadyAnalyzed.clear();
  NewClusterSelectedPads.clear();
  FinalSelCluster.clear();

  TotalSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TotalSelectedPads[z].resize(36);}
  TmpSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TmpSelectedPads[z].resize(36);}
  AlreadyAnalyzed.resize(48);
  for (int z=0; z < 48; ++z) {AlreadyAnalyzed[z].resize(36);}
  NewClusterSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {NewClusterSelectedPads[z].resize(36);}

  FinalSelCluster.resize(48);
  for (int z=0; z < 48; ++z) FinalSelCluster[z].resize(36);

  //The main loop of the DBSCAN iterates over all pads in the cleaned input sample to cluster them.

  for (int j = 0; j < 48; j++){
    for (int i = 0; i < 36; ++i) {

      if (PadDisplay[j][i] <= 0)  continue;
      if (TotalSelectedPads[j][i]) continue; // avoids clustering if the pad has been already included in a cluster

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }

      sel_pads = 0;
      if(sel_pads == 0) {NewClusterSelectedPads[j][i] = PadDisplay[j][i]; sel_pads = 1;}// starting pad for the new cluster

      pads_ana=0;

      while (pads_ana < sel_pads){
        for (int l = 0; l < 48; l++) {
          for (int k = 0; k < 36; ++k) {

            if(!NewClusterSelectedPads[l][k]) continue; // only analyse pads in the new cluster
            if(AlreadyAnalyzed[l][k]) continue;  // ignore pads already considered for the new cluster

            pads_ana++;

            if(DEBUG) cout << endl << "analysing: " << l << " | " << k << endl << endl;

            AlreadyAnalyzed[l][k] = 1;
            t1 = PadTime[l][k];
            int cnt=0;

            for (int n = 0; n < 48; n++) {
              for (int m = 0; m < 36; ++m) {
                if (PadDisplay[n][m] <= 0)  continue;
                if (m == k && n ==l) continue;
                  t2 = PadTime[n][m];
                  distance =  pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5);
                if (distance < distMin) {
                  cnt++;
                  TmpSelectedPads[n][m]=1;
                  if(DEBUG) cout << n << " | " << m << "| dist: " << distance <<  endl;
                } // count how many pads are inside the 3D bubble
              }
            }

            if(DEBUG) cout << "Added to selection: " << cnt << endl;
            if(cnt>=numPadsCut){
              if(DEBUG) cout << "Clustered?: Yes!" << endl;
              sel_pads+=cnt;
              for (int q = 0; q < 48; q++) {
                for (int p = 0; p < 36; ++p) {
                  if(!TmpSelectedPads[q][p]) continue;
                  NewClusterSelectedPads[q][p] = 1;
                }
              }
            }
            if (DEBUG && cnt<numPadsCut) cout << "Clustered?: No!" << endl;

            sel_pads=0;
            for (int q = 0; q < 48; q++) {
              for (int p = 0; p < 36; ++p) {
                if(!NewClusterSelectedPads[q][p]) continue;
                sel_pads++;
              }
            }

            if(DEBUG) cout << "selected: " << sel_pads << endl;
            if(DEBUG) cout << "analysed: " << pads_ana << endl;
          }
        }
      }

      Npads.push_back(sel_pads);

      for (int s = 0; s < 48; s++) {
        for (int r = 0; r < 36; ++r) {
          if(!NewClusterSelectedPads[s][r]) {FinalSelCluster[s][r].push_back(-1); continue;}
          TotalSelectedPads[s][r] = 1;
          FinalSelCluster[s][r].push_back(PadDisplay[s][r]);
        }
      }

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }
    }
  }

  if (!Npads.size()) return false;

  if(!allowMULTITRACK){
    indexMax = 0;
    int maxNPads = -1;
    for(int g=0; g< (int) Npads.size(); g++){
      if(Npads[g] > maxNPads) {maxNPads = Npads[g]; indexMax = g;}
    }
  }
  else indexMax = -1;

  int selec = 0;
  for (int x=0; x< (int) Npads.size(); x++){

    if(allowMULTITRACK == false && x != indexMax) continue; // Disable multitrack, only take the track with more hits!

    selec=0;
    uint index = x;

    if (Npads[index] < 50 || Npads[index] > 250) continue;
    if(DEBUG) cout << "trackPads: " << Npads[index]  << endl;

    for (int jj = 0; jj < 48; jj++){
      for (int ii = 0; ii < 36; ++ii) {
        if(FinalSelCluster[jj][ii][index]>0){
          OutPad[jj][ii] = PadDisplay[jj][ii];
        }
      }
    }

    int n_pads = 0;
    TH2F* padHisto = new TH2F("padhisto", "", 36, 0., 36., 48, 0., 48.);
    for (int j = 0; j < 48; ++j) {
      for (int i = 0; i < 36; ++i) {
        if (OutPad[j][i]) {
          padHisto->SetBinContent(i+1, j+1, OutPad[j][i]);
          n_pads++;
        }
      }
    }

    int done = 0;
    (void)done;
    int n_col = 0;
    for (int i = 0; i < 36; ++i) {
      done = 0;
      for (int j = 0; j < 48; ++j) {
        if (OutPad[j][i]) {
          n_col++;
          if (abs(j-24)>20) selec++;
        }
        if(n_col>0 && !OutPad[j][i]) done = 1;
      }
      if (n_col > 7) selec++;
      n_col = 0;
    }

    padHisto->Fit("pol1", "Q");
    TF1* fit = padHisto->GetFunction("pol1");

    if (!fit){
      delete padHisto;
      return false;
    }

    double quality = fit->GetChisquare() / fit->GetNDF();
    double k = fit->GetParameter(1);
    double b = fit->GetParameter(0);

    delete padHisto;

    if (DEBUG) cout << "Fitted with a line k = " << k << " b = " << b << " quality = " << quality << endl;

    if(abs(k)>0.2 || quality > 200)  selec++;

    if(!selec) MultiOutPad.push_back(OutPad);

    for (int jj = 0; jj < 48; jj++){
      for (int ii = 0; ii < 36; ++ii){
        OutPad[jj][ii] = 0;
      }
    }

  }

  return true;
}


//*******************************************************
bool TimeFilterBeam(vector< vector<short> > &PadSelected, vector< vector<short> > &PadTime, vector< vector<short> > PadDisplay, vector< vector<vector<short> > > PadDisplayV) {
//*******************************************************

  int sel_pads = -1;
  int distance = -1;
  int pads_ana = -1;
  int t1= -1;
  int t2= -1;
  int distMin = 5;
  int numPadsCut = 1;
  double fact = 12.5;
  int addittions = 0;

 // TH2F* h_sel_clusters[256];
  vector<int> Npads;

  Npads.clear();

  //in the coments of this selection "pad" should be understood as a "3D pad", with coordinates (j,i,PadTime[j][i])
  //reminder: PadTime[j][i] contains the time asociated to the maxADC, this can introduce noise in case of track overlapping.
  //accordingly a more delicate treatment of time should be done in the future.

  // First we clean the input sample of pads with charge below ChargeCut or produce before minTime or after maxTime;

 if(DEBUG) cout << endl << "NEW EVENT" << endl << endl;

  vector< vector<int> > TotalSelectedPads;
  vector< vector<int> > TmpSelectedPads;
  vector< vector<int> > AlreadyAnalyzed;
  vector< vector<int> >  NewClusterSelectedPads;
  vector< vector<vector<int> > > FinalSelCluster;

  TotalSelectedPads.clear();
  TmpSelectedPads.clear();
  AlreadyAnalyzed.clear();
  NewClusterSelectedPads.clear();
  FinalSelCluster.clear();

  TotalSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TotalSelectedPads[z].resize(36);}
  TmpSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {TmpSelectedPads[z].resize(36);}
  AlreadyAnalyzed.resize(48);
  for (int z=0; z < 48; ++z) {AlreadyAnalyzed[z].resize(36);}
  NewClusterSelectedPads.resize(48);
  for (int z=0; z < 48; ++z) {NewClusterSelectedPads[z].resize(36);}

  FinalSelCluster.resize(48);
  for (int z=0; z < 48; ++z) FinalSelCluster[z].resize(36);

  //The main loop of the DBSCAN iterates over all pads in the cleaned input sample to cluster them.

  for (int j = 0; j < 48; j++){
    for (int i = 0; i < 36; ++i) {

      if (PadSelected[j][i] <= 0)  continue;
      if (TotalSelectedPads[j][i]) continue; // avoids clustering if the pad has been already included in a cluster

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }

      sel_pads = 0;
      if(sel_pads == 0) {NewClusterSelectedPads[j][i] = PadSelected[j][i]; sel_pads = 1;}// starting pad for the new cluster

      pads_ana=0;

      while (pads_ana < sel_pads){
        for (int l = 0; l < 48; l++) {
          for (int k = 0; k < 36; ++k) {

            if(!NewClusterSelectedPads[l][k]) continue; // only analyse pads in the new cluster
            if(AlreadyAnalyzed[l][k]) continue;  // ignore pads already considered for the new cluster

            pads_ana++;

            if(DEBUG) cout << endl << "analysing: " << l << " | " << k << endl << endl;

            AlreadyAnalyzed[l][k] = 1;
            t1 = PadTime[l][k];
            int cnt=0;

            for (int n = 0; n < 48; n++) {
              for (int m = 0; m < 36; ++m) {
                if (PadDisplay[n][m] <= 0)  continue;
                if (m == k && n ==l) continue;
                  t2 = PadTime[n][m];
                  if(PadSelected[n][m] > 0 ) distance =  pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5);
                  else{
                    if (DEBUG) cout << "dist:" << pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5) << endl;

                    if (DEBUG) cout << "time diff: " << abs(t1-t2) << endl;
                    if(pow(pow(abs(m-k),2)+pow(abs(n-l),2),0.5) > 1.1) continue;

                    int tWindow = 40;
                    if(abs(t1-t2)>60){
                      int maxt2 = 0;
                      int it_t2 = 0;
                      for (int qq=t1-tWindow/2; qq<=t1+tWindow/2; qq++){
                        if(!(t2-tWindow/2 > 10  && t2+tWindow/2 < 490 )) continue;
                        if(PadDisplayV[n][m][qq] > maxt2) {maxt2 = PadDisplayV[n][m][qq]; it_t2 = qq;}
                      }
                      if (it_t2 != 0){
                        if (DEBUG)  cout << "t2 change from " << t2 << " to: " << it_t2 << endl;
                        if (DEBUG)  cout << "Q change from " << PadDisplayV[n][m][t2] << " to: " << PadDisplayV[n][m][it_t2] << endl;
                        PadDisplay[n][m] = PadDisplayV[n][m][it_t2];
                        PadTime[n][m] = it_t2;
                        t2 = it_t2;
                        addittions++;
                      }
                      distance =  pow(pow(abs(m-k),2)+pow(abs(n-l),2)+pow(abs(t1-t2)/fact,2),0.5);
                    if (DEBUG)  if(distance < distMin) cout << "MODIFIED in Filter at:  i = " << m << " j = " << n << endl;
                    }
                  }
                if (distance < distMin) {
                  cnt++;
                  TmpSelectedPads[n][m]=1;
                  if(DEBUG) cout << n << " | " << m << "| dist: " << distance <<  endl;
                } // count how many pads are inside the 3D bubble
              }
            }

            if(DEBUG) cout << "Added to selection: " << cnt << endl;
            if(cnt>=numPadsCut){
              if(DEBUG) cout << "Clustered?: Yes!" << endl;
              sel_pads+=cnt;
              for (int q = 0; q < 48; q++) {
                for (int p = 0; p < 36; ++p) {
                  if(!TmpSelectedPads[q][p]) continue;
                  NewClusterSelectedPads[q][p] = 1;
                }
              }
            }
            if (DEBUG && cnt<numPadsCut) cout << "Clustered?: No!" << endl;

            sel_pads=0;
            for (int q = 0; q < 48; q++) {
              for (int p = 0; p < 36; ++p) {
                if(!NewClusterSelectedPads[q][p]) continue;
                sel_pads++;
              }
            }

            if(DEBUG) cout << "selected: " << sel_pads << endl;
            if(DEBUG) cout << "analysed: " << pads_ana << endl;
          }
        }
      }

      Npads.push_back(sel_pads);

      for (int s = 0; s < 48; s++) {
        for (int r = 0; r < 36; ++r) {
          if(!NewClusterSelectedPads[s][r]) {FinalSelCluster[s][r].push_back(-1); continue;}
          TotalSelectedPads[s][r] = 1;
          FinalSelCluster[s][r].push_back(PadDisplay[s][r]);
        }
      }

      for (int jj = 0; jj < 48; jj++){
        for (int ii = 0; ii < 36; ++ii) {
          NewClusterSelectedPads[jj][ii] = 0;
          TmpSelectedPads[jj][ii] = 0;
          AlreadyAnalyzed[jj][ii] = 0;
        }
      }
    }
  }


  uint index = 0;
  uint selec = 0;

  if (Npads[index] < 50) index = 1;
  if (Npads[index] < 50) index = 2;
  if (Npads[index] < 50) index = 3;

  if (Npads[index] < 50 || Npads[index] > 250) return false;
  if(DEBUG) cout << "trackPads: " << Npads[index]  << endl;

  for (int jj = 0; jj < 48; jj++){
    for (int ii = 0; ii < 36; ++ii) {
      if(FinalSelCluster[jj][ii][index]>0){
        PadSelected[jj][ii] = PadDisplay[jj][ii];
      }
    }
  }

  int n_pads = 0;
  TH2F* padHisto = new TH2F("padhisto", "", 36, 0., 36., 48, 0., 48.);
  for (int j = 0; j < 48; ++j) {
    for (int i = 0; i < 36; ++i) {
      if (PadSelected[j][i]) {
        padHisto->SetBinContent(i+1, j+1, PadSelected[j][i]);
        n_pads++;
      }
    }
  }

  int potDanger = -1;
  int dLast = -99;
  int dNext = -99;
  int nholes = 0;

  int done = 0;
  int n_col = 0;
  int t_cnt = 0;
  for (int i = 0; i < 36; ++i) {
    done = 0;
    potDanger = -1;
    for (int j = 0; j < 48; ++j) {
      if (PadSelected[j][i]) {
        n_col++;
        dLast = -99;
        dNext = -99;
        if (j!=0) {if (PadSelected[j][i]>0 && PadSelected[j-1][i] ==0) dLast = -1;}
        if (j!=47) {if (PadSelected[j][i]>0 && PadSelected[j+1][i] ==0) dNext = 1;}
        if(dLast==-1 && dNext==1) potDanger = j;
        if (abs(j-24)>12) selec++;
        if (PadTime[j][i] > 470 || PadTime[j][i] < 20) t_cnt++;
      }
      if(n_col>0 && !PadSelected[j][i]) done = 1;
      if(done == 1 && PadSelected[j][i]>0 && potDanger == -1) selec++;
      if(done == 1 && PadSelected[j][i]>0 && potDanger >= 0 && potDanger < 48 && n_col > 2) PadSelected[potDanger][i]=0;
      if(done == 1 && PadSelected[j][i]>0 && potDanger >= 0 && potDanger < 48 && n_col <= 2) selec++;

      if(j == 47 && n_col == 0) nholes++;
    }
    if (n_col > 7) selec++;
    n_col = 0;
  }

  if (nholes >= 3) selec++;
  if(t_cnt > 15) selec++;

  padHisto->Fit("pol1", "Q");
  TF1* fit = padHisto->GetFunction("pol1");

  if (!fit){
    delete padHisto;
    return false;
  }

  double quality = fit->GetChisquare() / fit->GetNDF();
  double k = fit->GetParameter(1);
  double b = fit->GetParameter(0);

  delete padHisto;

  if (DEBUG) cout << "Fitted with a line k = " << k << " b = " << b << " quality = " << quality << endl;

  if(abs(k)>0.2 || quality > 200)  selec++;

  if(selec > 0) return false;
  if(addittions > 3) return false;

  if (DEBUG) cout << "addittions: " << addittions << endl;
  if (DEBUG) cout << "Filter Success!" << endl;
  return true;
}
