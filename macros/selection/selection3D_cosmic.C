#include <iostream>
#include <vector>
using namespace std;

#include "line.h"
#include "../ilc/common_header.h"

bool TestSelection3D_cosmic(vector< vector<short> >& PadDisplay, vector< vector<short> >& PadTime,
                   vector< vector<short> >& OutPad, vector<vector< vector<short> > >& MultiOutPad,
                   vector<vector< vector<short> > >& MultiOutPadTime,
                   vector< vector< vector<short> > >& PadDisplay3D) {
  int cluster_offset = 3;

  int cluster_threshold   = 40;
  int cluster_time_gate   = 30;
  int cluster_space_gate  = 3;

  int time_gate = 20;

  int box_width_t2    = time_gate;
  int box_width_t1    = time_gate;
  int scan_time_up    = time_gate;
  int scan_time_down  = time_gate;
  int box_width_j  = 3;

  int track_collection_dist = 4;

  int breaking_thr  = 0;  // max number of gaps
  int column_thr    = 35; // min number of columns
  //int OOT_thr       = 20; // suppress cosmic pile up

  uint cluster_lim  = 7;

  bool pile_up_cut  = false;

  float tang_cut    = 0.9;

  vector<float> front_cluster_t;
  vector<float> front_cluster_j;
  vector<float> front_cluster_i;
  vector<int>   front_cluster_N;
  vector<int>   front_cluster_charge;

  // looping over all side pads to find time & space cluster
  for (int i = 0; i < 36; ++i) {
    for (int j = 0; j < cluster_offset; ++j) {
      if (PadDisplay[j][i] <= cluster_threshold)
        continue;

      int index = -1;
      for (uint it = 0; it < front_cluster_t.size(); ++it) {
        if (abs(PadTime[j][i] - front_cluster_t[it])  < cluster_time_gate
         && abs(j             - front_cluster_j[it]) < cluster_space_gate ) {
          index = it;
          break;
        }
      }

      if (index == -1) {
        front_cluster_t.push_back(1. * PadTime[j][i]);
        front_cluster_j.push_back((cluster_offset - 1) / 2);
        front_cluster_i.push_back(1. * i);
        front_cluster_N.push_back(1);
        front_cluster_charge.push_back(PadDisplay[j][i]);
      } else {
        // assign to existing cluster
        // average the space anf time
        front_cluster_t[index] = 1. * front_cluster_t[index] * front_cluster_N[index] + PadTime[j][i];
        front_cluster_i[index] = 1. * front_cluster_i[index] * front_cluster_N[index] + i;
        ++front_cluster_N[index];
        front_cluster_t[index] /= front_cluster_N[index];
        front_cluster_i[index] /= front_cluster_N[index];
        front_cluster_charge[index] += PadDisplay[j][i];
      }
    } // over i within cluster_offset
  } // over j

  if (front_cluster_t.size() > cluster_lim || !front_cluster_t.size()) {
    vector<float>().swap(front_cluster_t);
    vector<float>().swap(front_cluster_j);
    vector<float>().swap(front_cluster_i);
    vector<int>().swap(front_cluster_N);
    return false;
  }

  vector<float> back_cluster_t;
  vector<float> back_cluster_j;
  vector<float> back_cluster_i;
  vector<int>   back_cluster_N;
  vector<int>   back_cluster_charge;

  for (int i = 0; i < 36; ++i) {
    for (int j = 48 - cluster_offset; j < 48; ++j) {
      if (PadDisplay[j][i] <= cluster_threshold)
        continue;

      int index = -1;
      for (uint it = 0; it < back_cluster_t.size(); ++it) {
        if (abs(PadTime[j][i] - back_cluster_t[it])  < cluster_time_gate
         && abs(j             - back_cluster_j[it]) < cluster_space_gate ) {
          index = it;
          break;
        }
      }

      if (index == -1) {
        // create new cluster
        back_cluster_t.push_back(1. * PadTime[j][i]);
        back_cluster_j.push_back(47 - (cluster_offset - 1) / 2);
        back_cluster_i.push_back(1. * i);
        back_cluster_N.push_back(1);
        back_cluster_charge.push_back(PadDisplay[j][i]);
      } else {
        // assign to existing cluster
        // average the space anf time
        back_cluster_t[index] = 1. * back_cluster_t[index] * back_cluster_N[index] + PadTime[j][i];
        back_cluster_i[index] = 1. * back_cluster_i[index] * back_cluster_N[index] + i;
        ++back_cluster_N[index];
        back_cluster_t[index] /= back_cluster_N[index];
        back_cluster_i[index] /= back_cluster_N[index];
        back_cluster_charge[index] += PadDisplay[j][i];
      }
    } // over i within cluster_offset
  } // over j

  if (back_cluster_t.size() > cluster_lim || !back_cluster_t.size()) {
    vector<float>().swap(back_cluster_t);
    vector<float>().swap(back_cluster_j);
    vector<float>().swap(back_cluster_i);
    vector<int>().swap(back_cluster_N);
    return false;
  }

  // store the tracks info
  vector<vector<vector<short> > > track_container;
  vector<vector<vector<short> > > track_container_time;
  vector<int> track_col;

  if (DEBUG) {
    cout << "cluster size " << front_cluster_t.size() << "   by   " << back_cluster_t.size() << endl;
  }

  bool pileup = false;

  // loop over all possible combinations
  for (uint trackStart = 0; trackStart < front_cluster_t.size(); ++trackStart) {
    for (uint trackEnd = 0; trackEnd < back_cluster_t.size(); ++trackEnd) {

      if (DEBUG)
        cout << "working " << trackStart << " " << trackEnd << endl;

      // create a draft for tarck
      vector<vector<short> > TrackDraft;
      vector<vector<short> > TrackDraft_time;
      TrackDraft.resize(47+1);
      TrackDraft_time.resize(47+1);
      for (int z=0; z <= 47; ++z) {
        TrackDraft[z].resize(35+1, 0);
        TrackDraft_time[z].resize(35+1, 0);
      }

      TVector3 vec1 = TVector3(front_cluster_j[trackStart], front_cluster_i[trackStart], front_cluster_t[trackStart]);
      TVector3 vec2 = TVector3(back_cluster_j[trackEnd], back_cluster_i[trackEnd], back_cluster_t[trackEnd]);
      TLine_att line(vec1, vec2);

      // interested only in hrisontal tracks
      if (abs(line.GetDir().X()) < tang_cut)
        continue;


      // centered tracks
      //if (abs(front_cluster_j[trackStart] - 23.5) > 1 || abs(back_cluster_j[trackEnd] - 23.5) > 1)
      //  continue;

      // tracks away from centre
      //if (!((front_cluster_j[trackStart] > 29 && back_cluster_j[trackEnd] > 29) ||
      //    (front_cluster_j[trackStart] < 18 && back_cluster_j[trackEnd] < 18)))
      //  continue;

      if (DEBUG) {
        cout << "vec1" << endl;
        cout << vec1.X() << "\t" << vec1.Y() << "\t" << vec1.Z() << endl;
        cout << "vec2" << endl;
        cout << vec2.X() << "\t" << vec2.Y() << "\t" << vec2.Z() << endl;

        cout << "pos" << endl;
        cout << line.GetPos().X() << "\t"  << line.GetPos().Y() << "\t" << line.GetPos().Z() << "\t" << endl;
        cout << "dir" << endl;
        cout << line.GetDir().X() << "\t"  << line.GetDir().Y() << "\t" << line.GetDir().Z() << "\t" << endl;
      }

      // scan the pad with the threshold and within a box
      // count number of columns
      int columns = 0;

      int breaking = 0;
      bool gap = false;

      // pads out of time but above threshold
      int pad_OOT = 0;

      for (int it_j = 0; it_j < 48; ++it_j) {
        bool column = false;
        int track_max   = -1;
        int track_max_i = -1;
        int track_max_t = -1;
        bool columan_max_adc = false;
        (void)columan_max_adc;

        for (int it_i = 0; it_i < 36; ++it_i) {

          if (!PadDisplay[it_j][it_i])
            continue;

          TVector3 track_pos = line.EvalX(it_j);

          int found_max       = -1;
          int found_max_time  = -1;

          if (abs(track_pos.Z() - PadTime[it_j][it_i]) <= box_width_t1) {
            found_max       = PadDisplay[it_j][it_i];
            found_max_time  =  PadTime[it_j][it_i];
            columan_max_adc = true;
          } else {
            int first_time    = max((int)track_pos.Z() - scan_time_down, TIME_FIRST);
            int last_time     = min((int)track_pos.Z() + scan_time_up, TIME_LAST);

            int it_t = first_time;
            /*for (it_t = first_time; it_t <= last_time; ++it_t) {

              if (!PadDisplay3D[it_j][it_i][it_t])
                continue;

              if (PadDisplay3D[it_j][it_i][it_t] > found_max) {
                found_max       = PadDisplay3D[it_j][it_i][it_t];
                found_max_time  = it_t;
              }
            } // loop over time*/
            while (it_t < TIME_LAST) {
              if (found_max && !PadDisplay3D[it_j][it_i][it_t])
                break;

              if (found_max == -1 && it_t > last_time)
                break;

              if (PadDisplay3D[it_j][it_i][it_t] > found_max) {
                found_max       = PadDisplay3D[it_j][it_i][it_t];
                found_max_time  = it_t;
              }

              ++it_t;
            } // loop over time
          }

          if (found_max == -1)
            continue;

          if (abs(track_pos.Y() - it_i) > box_width_j) {
            ++pad_OOT;
            continue;
          }

          if (found_max > track_max) {
            track_max   = found_max;
            track_max_i = it_i;
            track_max_t = found_max_time;
            column = true;
          }
        } // over j

        //if (!columan_max_adc)
        //  pileup = true;

        if (column) {
          ++columns;
          TrackDraft[it_j][track_max_i]       = track_max;
          TrackDraft_time[it_j][track_max_i]  = track_max_t;
          if (gap) {
            ++breaking;
            gap = false;
          }
        } else if (columns)
          gap = true;
      } // over i

      if (DEBUG)
        cout << "break " <<  breaking << "   col " << columns << "  OOT " << pad_OOT << endl;

      // check number of columns and number of gaps
      if (breaking > breaking_thr || columns < column_thr)// || pad_OOT > OOT_thr)
        continue;

      if (DEBUG)
        cout << "passed" << endl;

      if (pileup && pile_up_cut)
        continue;

      track_container.push_back(TrackDraft);
      track_container_time.push_back(TrackDraft_time);
      track_col.push_back(columns);
    } // track end
  } // track start

  if (!track_container.size())
    return false;

  if (DEBUG)
    cout << "container size " << track_container.size() << endl;

  for (uint trackId = 0; trackId < track_container.size(); ++trackId) {
    for (int it_j = 0; it_j < 48; ++it_j) {
      int it_i = 0;
      while (!track_container[trackId][it_j][it_i] && it_i < 36)
        ++it_i;

      if (it_i == 36)
        continue;

      int it_i_track = it_i;

      int first_time    = max(PadTime[it_j][it_i_track] - scan_time_down, TIME_FIRST);
      int last_time     = min(PadTime[it_j][it_i_track] + scan_time_up, TIME_LAST);

      while (PadDisplay[it_j][it_i] && it_i < 36
        && abs(it_i - it_i_track) < track_collection_dist) {

        // if the maximum is inside the time box --> take it
        if(PadTime[it_j][it_i] - PadTime[it_j][it_i_track] < box_width_t2 && PadTime[it_j][it_i_track] - PadTime[it_j][it_i] < box_width_t1) {
          track_container[trackId][it_j][it_i]      = PadDisplay[it_j][it_i];
          track_container_time[trackId][it_j][it_i] = PadTime[it_j][it_i];
        // if not --> scan ADC vs. time for this pad
        } else {
          int adc_max_inbox = -1;
          int adc_max_t     = -1;

          /*for (int it_t = first_time; it_t <= last_time; ++it_t) {

            if (!PadDisplay3D[it_j][it_i][it_t])
              continue;

            if (PadDisplay3D[it_j][it_i][it_t] > adc_max_inbox) {
              adc_max_inbox = PadDisplay3D[it_j][it_i][it_t];
              adc_max_t     = it_t;
            }
          } // loop over time*/
          int it_t = first_time;
          while (it_t < TIME_LAST) {
            if (adc_max_inbox && !PadDisplay3D[it_j][it_i][it_t])
              break;

            if (adc_max_inbox == -1 && it_t > last_time)
               break;

            if (PadDisplay3D[it_j][it_i][it_t] > adc_max_inbox) {
               adc_max_inbox       = PadDisplay3D[it_j][it_i][it_t];
               adc_max_t  = it_t;
             }
             ++it_t;
          } // loop over time

          // if the hit in the box is befor the max hit --> OK
          // if not --> check that we are taking the peak not the tail
          if (adc_max_t > PadTime[it_j][it_i] && adc_max_t - PadTime[it_j][it_i] < scan_time_up && pile_up_cut) {
            pileup = true;
            break;
          }
          if (adc_max_inbox != -1) {
            track_container[trackId][it_j][it_i]      = adc_max_inbox;
            track_container_time[trackId][it_j][it_i] = adc_max_t;
          } else {
            if (PadDisplay[it_j][it_i] > cluster_threshold && pile_up_cut) {
              pileup = true;
            }
            break;
          }
        } // look for the hit in time window
        ++it_i;
      } // go up

      it_i = it_i_track;
      while (PadDisplay[it_j][it_i] && it_i > 0
        && abs(it_i - it_i_track) < track_collection_dist) {
        // if the maximum is inside the time box --> take it
        if(PadTime[it_j][it_i] - PadTime[it_j][it_i_track] < box_width_t2 && PadTime[it_j][it_i_track] - PadTime[it_j][it_i] < box_width_t1) {
          track_container[trackId][it_j][it_i] = PadDisplay[it_j][it_i];
          track_container_time[trackId][it_j][it_i] = PadTime[it_j][it_i];
        // if not --> scan ADC vs. time for this pad
        } else {
          int adc_max_inbox = -1;
          int adc_max_t     = -1;
          /*for (int it_t = first_time; it_t <= last_time; ++it_t) {

            if (!PadDisplay3D[it_j][it_i][it_t])
              continue;

            if (PadDisplay3D[it_j][it_i][it_t] > adc_max_inbox) {
              adc_max_inbox = PadDisplay3D[it_j][it_i][it_t];
              adc_max_t     = it_t;
            }
          } // loop over time*/

          int it_t = first_time;
          while (it_t < TIME_LAST) {
            if (adc_max_inbox && !PadDisplay3D[it_j][it_i][it_t])
              break;

            if (adc_max_inbox == -1 && it_t > last_time)
               break;

            if (PadDisplay3D[it_j][it_i][it_t] > adc_max_inbox) {
               adc_max_inbox       = PadDisplay3D[it_j][it_i][it_t];
               adc_max_t  = it_t;
             }
             ++it_t;
           } // loop over time

          // if the hit in the box is befor the max hit --> OK
          // if not --> check that we are taking the peak not the tail
          if (adc_max_t > PadTime[it_j][it_i] && adc_max_t - PadTime[it_j][it_i] < scan_time_up && pile_up_cut) {
            pileup = true;
            break;
          }

          if (adc_max_inbox != -1) {
            track_container[trackId][it_j][it_i] = adc_max_inbox;
            track_container_time[trackId][it_j][it_i] = adc_max_t;
          } else {
            if (PadDisplay[it_j][it_i] > cluster_threshold && pile_up_cut) {
              pileup = true;
            }

            break;
          }
        } // look for the hit in time window
        --it_i;
      } // go down (j)
    if (pileup && pile_up_cut)
      break;
    } // i
    if (!pileup || !pile_up_cut) {
      MultiOutPad.push_back(track_container[trackId]);
      MultiOutPadTime.push_back(track_container_time[trackId]);
    }
  } // loop over tracks

  if (DEBUG)
    cout << "MultiOutPad size " << MultiOutPad.size() << endl;

  if (!MultiOutPad.size())
    return false;

  OutPad = MultiOutPad[0];

  return true;
}

bool TestSelection3D_cosmic(vector< vector<short> >& PadDisplay, vector< vector<short> >& PadTime,
                   vector< vector<short> >& OutPad, vector< vector< vector<short> > >& PadDisplay3D) {
  vector<vector< vector<short> > > MultiOutPad;
  vector<vector< vector<short> > > MultiOutPadTime;
  return TestSelection3D_cosmic(PadDisplay, PadTime, OutPad, MultiOutPad, MultiOutPadTime, PadDisplay3D);
}

bool TestSelection3D_cosmic(vector< vector<short> >& PadDisplay, vector< vector<short> >& PadTime,
                   vector< vector<short> >& OutPad, vector<vector< vector<short> > >& MultiOutPad,
                   vector< vector< vector<short> > >& PadDisplay3D) {
  vector<vector< vector<short> > > MultiOutPadTime;
  return TestSelection3D_cosmic(PadDisplay, PadTime, OutPad, MultiOutPad, MultiOutPadTime, PadDisplay3D);
}