#define THIS_NAME dedx_resol
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "ilc/common_header.h"
#include "selection/selection3D.C"
#include "selection/DBSCAN.C"

//#define COSMIC
#define PEAK2

//#define SEVERE_SUPPRESSION
bool SUPPRESS_ERRORS = true;

void dedx_resol() {

  #ifdef SEVERE_SUPPRESSION
    gErrorIgnoreLevel = kSysError;
  #endif
    
  vector<TString> listOfFiles;
  TString file    = "default.root";
  TString prefix  = "~/";
  for (int iarg=0; iarg<gApplication->Argc(); iarg++){
    if (string( gApplication->Argv(iarg))=="-f" || string( gApplication->Argv(iarg))=="--file" ){
      iarg++;
      file = gApplication->Argv(iarg);
      if (file.Contains(".root")) {
        cout << "adding filename" <<" " << file << endl;
        listOfFiles.push_back(file);
      } 
      else {
        fstream fList(file);
        if (fList.good()) {
          while (fList.good()) {
            string filename;
            getline(fList, filename);
            if (fList.eof()) break;
            listOfFiles.push_back(filename);
          }
        }
      }
    }
    /* 
    else if (string( gApplication->Argv(iarg))=="-o" || string( gApplication->Argv(iarg))=="--output" ){
      iarg++;
      prefix = gApplication->Argv(iarg);
    } 
    else if (string( gApplication->Argv(iarg))=="-h" || string( gApplication->Argv(iarg))=="--help" ){
      cout << "**************************************" << endl;
      cout << "Macros run options:" << endl;
      cout << "   -f || --file      input file (*.root) or filelist" << endl;
      cout << "   -o || --output    output folder for plots and out file" << endl;
      cout << "   -h || --help      print help info" << endl;
      cout << "**************************************" << endl;
    }*/
  }

  TString file_name = TString(file);
  int m=0., i_max1;
  double alpha = 0.7;
  float mean[18], RMS[18], valor[18], mean1[18], RMS1[18], valor1[18], norm_cluster1;
  int n_b[18];
  char gname[60], fname[60];    
  TString prefix3 = "/afs/cern.ch/work/d/dvargas/public/TPC-Dana/FIG/";
  
  TH1F* ClusterCharge     = new TH1F("cluster_charge","Cluster charge",150,0,3000);
  TH1F* ClusterChargeCur  = new TH1F("cluster_chargeC","Cluster charge current",150,0,3000);
  TH2F* PadDisplayHIT     = new TH2F("PadDisplayH","I vs J of hits",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayMAX     = new TH2F("PadDisplayM","I vs J of hits",38,-1.,37.,50,-1.,49.);
  
  int Inter=0;                                                     
  TGraph * dedx_cal = new TGraph();                                      
  dedx_cal->SetTitle("dE/dx resolution (with truncated mean 70%) one gaus; n; #sigma(dE/dx)/(dE/dx) (%) ; dedx_cal"); 
  int Inter1=0;                                                     
  TGraph * dedx_cal1 = new TGraph();                                      
  dedx_cal1->SetTitle("dE/dx resolution (with truncated mean 70%) two gaus; n; #sigma(dE/dx)/(dE/dx) (%) ; dedx_cal");     

  TH1F *Q[18];
  for (int o=0;o<18;o++) {
    #ifdef COSMIC
      sprintf(gname,"for_n=48-(2*%d)",o);                               ///cosmic
    #else
      sprintf(gname,"for_n=36-(2*%d)",o);                               ///beam
    #endif
    Q[o] = new TH1F(gname,"; Charge; entries",500,0,1000);
    Q[o]->SetLineColor(4);
  }
  TH1F *Q1[18];
  for (int o=0;o<18;o++) {
    #ifdef COSMIC
      sprintf(fname,"for_n=48-(2*%d)",o);                               ///cosmic
    #else
      sprintf(fname,"for_n=36-(2*%d)",o);                               ///beam
    #endif
    Q1[o] = new TH1F(fname,"; Charge; entries",500,0,1000);
    Q1[o]->SetLineColor(3);
  }
  
  Int_t total_events = 0;
  Int_t sel_events = 0;
  Int_t sel_events_tfilter = 0;

  vector< vector<short> > PadDisplay;
  vector< vector<short> > PadTime;
  vector< vector<short> > PadTimeMod;
  vector< vector<short> > PadDisplayNew;
  vector< vector<int> > PadIntegral;
  vector< vector<vector<short> > > PadDisplayV;
  vector< vector< vector<short> > > PadDisplay3D;

  //************************************************************
  //****************** LOOP OVER FILES  ************************
  //************************************************************
  vector< vector<int > > EventClusters;

  TFile*f=0;
  for (uint ifile=0;ifile< listOfFiles.size();ifile++){
    file = listOfFiles[ifile];
    if (f!=0) f->Close();
    cout << "opening file " << file <<  endl;
    f= new TFile(file);
    if (!f->IsOpen()) {
      cout << " WARNING problem in opening file " << file << endl;
      return;
    }

    vector<int> *iPad(0);
    vector<int> *jPad(0);
    vector<double> *xPad(0);
    vector<double> *yPad(0);

    vector<double> *dxPad(0);
    vector<double> *dyPad(0);

    TTree * tgeom= (TTree*) f->Get("femGeomTree");
    tgeom->SetBranchAddress("jPad", &jPad );
    tgeom->SetBranchAddress("iPad", &iPad );

    tgeom->SetBranchAddress("xPad", &xPad );
    tgeom->SetBranchAddress("yPad", &yPad );

    tgeom->SetBranchAddress("dxPad", &dxPad );
    tgeom->SetBranchAddress("dyPad", &dyPad );
    tgeom->GetEntry(0); // put into memory geometry info
    cout << "Reading channel mapping" << endl;
    cout << "jPad->size()  " << jPad->size() <<endl;
    cout << "iPad->size()  " << iPad->size() <<endl;

    cout << "Reading geometry" << endl;
    cout << "xPad->size()  " << xPad->size() <<endl;
    cout << "yPad->size()  " << yPad->size() <<endl;
    cout << "dxPad->size() " << dxPad->size() <<endl;
    cout << "dyPad->size() " << dyPad->size() <<endl;

    int Imax = -1;
    int Imin = 10000000;
    int Jmax = -1;
    int Jmin = 10000000;

    for (unsigned long i = 0; i < jPad->size(); ++i) {
      if (Imax < (*iPad)[i])
        Imax = (*iPad)[i];
      if (Imin > (*iPad)[i])
        Imin = (*iPad)[i];
      if (Jmax < (*jPad)[i])
        Jmax = (*jPad)[i];
      if (Jmin > (*jPad)[i])
        Jmin = (*jPad)[i];
    }

    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    int Nevents=0;
    TTree *t;
    t = (TTree*) f->Get("padData");
    vector<short>          *listOfChannels(0);
    vector<vector<short> > *listOfSamples(0);

    t->SetBranchAddress("PadphysChannels", &listOfChannels );
    t->SetBranchAddress("PadADCvsTime"   , &listOfSamples );

    if (Nevents<=0) Nevents=t->GetEntries();
    if (Nevents>t->GetEntries()) Nevents=t->GetEntries();

    cout << "[                                                  ] Nevents = "<<Nevents<<"\r[";

    //************************************************************
    //****************** LOOP OVER EVENTS ************************
    //************************************************************
    for (int ievt=0; ievt < Nevents ; ievt++){
      if (ievt%(Nevents/50)==0)
        cout <<"."<<flush;

      t->GetEntry(ievt);

      PadDisplayNew.clear();
      PadDisplay.clear();
      PadIntegral.clear();
      PadTime.clear();
      PadTimeMod.clear();

      PadDisplay.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplay[z].resize(Imax+1, 0);

      PadTime.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadTime[z].resize(Imax+1, 0);
    
      PadTimeMod.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadTimeMod[z].resize(Imax+1, 0);

      PadDisplayNew.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayNew[z].resize(Imax+1, 0);

      PadIntegral.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadIntegral[z].resize(Imax+1, 0);

      PadDisplayV.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayV[z].resize(Imax+1);

      ClusterChargeCur->Reset();

      //************************************************************
      //*****************LOOP OVER CHANNELS ************************
      //************************************************************
      for (uint ic=0; ic< listOfChannels->size(); ic++){
        int chan= (*listOfChannels)[ic];
        // find out the maximum
        float adcmax=-1;
        Int_t it_max = -1;

        // one maximum per channel
        for (uint it = 14; it < 480; it++){
        //for (uint it = 10; it < 450; it++){
          int adc= (*listOfSamples)[ic][it];
          if (adc>adcmax && adc > 0) {
            adcmax = adc;
            it_max = it;
          }
        }

        int lower = max (it_max - 10, 5);
        int upper = min(it_max + 20, 490);
        int integral = 0;
        for (int it = lower; it < upper; it++){
          // stop integrating if noise
          if ((*listOfSamples)[ic][it] < 0) {
            if (it > it_max)
              break;
            else continue;
          }

          integral += (*listOfSamples)[ic][it];
        }

        PadDisplayV[(*jPad)[chan]][(*iPad)[chan]] = (*listOfSamples)[ic];

        if (adcmax<0) continue; // remove noise

        PadDisplay[(*jPad)[chan]][(*iPad)[chan]]  = adcmax;
        PadTime[(*jPad)[chan]][(*iPad)[chan]]     = it_max;
        PadTimeMod[(*jPad)[chan]][(*iPad)[chan]]  = it_max;
        PadIntegral[(*jPad)[chan]][(*iPad)[chan]] = integral;
      } //loop over channels

      ++total_events;

      #ifndef SEVERE_SUPPRESSION
        Int_t gErrorIgnoreLevel_bu = kSysError;
        if (SUPPRESS_ERRORS) {
          gErrorIgnoreLevel_bu = gErrorIgnoreLevel;
          gErrorIgnoreLevel = kSysError;
        }
      #endif
      // apply the selection
      vector<vector< vector<short> > > MultiOutPad;

      #ifdef COSMIC
        if (!DBSCANSelectionCosmic(PadDisplay, PadTime, PadDisplayNew, MultiOutPad))  continue;
      #else
        if (!DBSCANSelectionBeam(PadDisplay, PadTime, PadDisplayNew, MultiOutPad)) continue;
      #endif

        ++sel_events;
        
      #ifndef SEVERE_SUPPRESSION
        if (SUPPRESS_ERRORS) gErrorIgnoreLevel = gErrorIgnoreLevel_bu;
      #endif

      for (uint trackId = 0; trackId < MultiOutPad.size() ; ++trackId) {

        #ifdef COSMIC
          if (!TimeFilterCosmic(MultiOutPad[trackId], PadTimeMod, PadDisplay, PadDisplayV)) continue;
        #else
          if (!TimeFilterBeam(MultiOutPad[trackId], PadTimeMod, PadDisplay, PadDisplayV)) continue;
        #endif
          
          ++sel_events_tfilter;
          vector<Float_t > cluster_charge;

        #ifdef COSMIC
          for (Int_t o = 0; o < 18; o++) {
            for (Int_t it_j = o; it_j < Jmax+1 - o; ++it_j) {
              n_b[o] = (Jmax+1) - (2*o);
        #else
          for (Int_t o = 0; o < 18; o++) {
            for (Int_t it_i = o; it_i < Imax+1 - o; ++it_i) {
              n_b[o] = (Imax+1) - (2*o);
        #endif

        Int_t cluster = 0;

          #ifdef COSMIC
            for (Int_t it_i = 0; it_i <= Imax; ++it_i) {
          #else
            for (Int_t it_j = 0; it_j <= Jmax; ++it_j) {
          #endif
              cluster += MultiOutPad[trackId][it_j][it_i];
              if (MultiOutPad[trackId][it_j][it_i] > 0) {
                PadDisplayHIT->Fill(it_i, it_j, 1);
              }
              PadDisplayMAX->Fill(it_i, it_j, MultiOutPad[trackId][it_j][it_i]);
            }//fast loop
            if (cluster != 0) {
              ClusterCharge->Fill(cluster);
              ClusterChargeCur->Fill(cluster);
              cluster_charge.push_back(cluster);
            }
          }
          // truncated mean
          sort(cluster_charge.begin(), cluster_charge.end());
          norm_cluster1 = 0.;
          i_max1 = round(alpha * cluster_charge.size());
          for (int i = 0; i < std::min(i_max1, int(cluster_charge.size())); ++i){
            norm_cluster1 += cluster_charge[i];   
          }
          norm_cluster1 *= 1 / (alpha * cluster_charge.size());
          Q[o]->Fill(norm_cluster1);
          Q1[o]->Fill(norm_cluster1);
          cluster_charge.clear();
        } //slow loop
      } // loop over selected tracks
    } // loop over events
  } // loop over files
  
  #ifdef COSMIC
    TFile* out_file = new TFile((prefix3+"dedx_resol_cosmic_" + file_name + ".root").Data(), "RECREATE");
  #else
    TFile* out_file = new TFile((prefix3+"dedx_resol_beam_" + file_name + ".root").Data(), "RECREATE");
  #endif

  TF1* f1 = new TF1("f1", "[0] * TMath::Gaus(x, [1], [2]) + [3] * TMath::Gaus(x, [4], [5])", 0, 1300);
  TF1 *gausfit = new TF1("gausfit","gaus",0, 1000);
  f1->SetParName(0, "Const");
  f1->SetParName(1, "Mean1");
  f1->SetParName(2, "sigma1");
  f1->SetParName(3, "comst2");
  f1->SetParName(4, "mean2");
  f1->SetParName(5, "sigma2");

  TCanvas *q_n = new TCanvas("q_n","Charge distribution 1 gauss", 0, 0, 1200, 800);
  q_n->Divide(6,3);
  for (int o=0; o<18;o++) {
    m=o+1;
    q_n->cd(m);
    Q[o]->Fit("gausfit", "R");
    Q[o]->Draw("COL");
    gPad->Update();
  }   
  q_n->Write();

  TCanvas *q_n1 = new TCanvas("q_n1","Charge distribution 2 gauss", 0, 0, 1200, 800);
  q_n1->Divide(6,3);
  for (int o=0; o<18;o++) {
    m=o+1;
    q_n1->cd(m);
    Q1[o]->Fit("f1", "R");
    Q1[o]->Draw("COL");
    gPad->Update();
  }   
  q_n1->Write();

  #ifdef COSMIC
    for (int o=0; o<18;o++) {
      Q1[o]->Fit("f1", "R");
      RMS1[o] = f1->GetParameter(2);
      mean1[o] = f1->GetParameter(1);                         
      valor1[o] = (RMS1[o]/mean1[o])*100;                               /// in porcent
      Q[o]->Fit("gausfit", "R");
      RMS[o] = gausfit->GetParameter(2);
      mean[o] = gausfit->GetParameter(1);                         
      valor[o] = (RMS[o]/mean[o])*100;                               /// in porcent
    }
  #else
    for (int o=0; o<18;o++) {
      Q1[o]->Fit("f1", "R");
      RMS1[o] = f1->GetParameter(2);
      mean1[o] = f1->GetParameter(1);                         
      valor1[o] = (RMS1[o]/mean1[o])*100;                               /// in porcent
      Q[o]->Fit("gausfit", "R");
      RMS[o] = gausfit->GetParameter(2);
      mean[o] = gausfit->GetParameter(1);                         
      valor[o] = (RMS[o]/mean[o])*100;                               /// in porcent
    }
  #endif
  for (int o=0; o<18;o++) {
    dedx_cal->SetPoint(Inter, n_b[o], valor[o]);
    Inter++;
    dedx_cal1->SetPoint(Inter1, n_b[o], valor1[o]);
    Inter1++;
  }

  #ifdef COSMIC
    TF1 *myfit = new TF1("myfit","[0]*pow(x,[1])", 14, 48);       ///cosmic
    TF1 *myfit1 = new TF1("myfit1","[0]*pow(x,[1])", 14, 48);       ///cosmic
  #else
    TF1 *myfit = new TF1("myfit","[0]*pow(x,[1])", 4, 36);   ///beam
    TF1 *myfit1 = new TF1("myfit1","[0]*pow(x,[1])", 4, 36);   ///beam
  #endif
  myfit->SetParName(0,"a");
  myfit->SetParName(1,"b");
  myfit->SetParameter(0, 0);
  myfit->SetParameter(1, -1);
  myfit->SetParLimits(0,0,100);
  myfit->SetParLimits(1,-1,0);

  myfit1->SetParName(0,"c");
  myfit1->SetParName(1,"d");
  myfit1->SetParameter(0, 0);
  myfit1->SetParameter(1, -1);
  myfit1->SetParLimits(0,0,100);
  myfit1->SetParLimits(1,-1,0);
   
  TCanvas *dedx_reolu = new TCanvas("dedx_reolu","dE/dx reolution gauss", 0, 0, 350, 350); 
  dedx_cal->GetYaxis()->SetRangeUser(0., 100.);
  #ifdef COSMIC
    dedx_cal->GetXaxis()->SetRangeUser(0., 50.); ///cosmic
  #else
    dedx_cal->GetXaxis()->SetRangeUser(0., 38.); ///beam
  #endif
  dedx_reolu->cd(1);
  dedx_cal->Fit("myfit", "R");
  dedx_cal->Draw("A *");
  dedx_reolu->Write();

  TCanvas *dedx_reolu1 = new TCanvas("dedx_reolu","dE/dx reolution 2 gauss", 0, 0, 350, 350); 
  dedx_cal1->GetYaxis()->SetRangeUser(0., 100.);
  #ifdef COSMIC
    dedx_cal1->GetXaxis()->SetRangeUser(0., 50.); ///cosmic
  #else
    dedx_cal1->GetXaxis()->SetRangeUser(0., 38.); ///beam
  #endif
  dedx_reolu1->cd(1);
  dedx_cal1->Fit("myfit1", "R");
  dedx_cal1->Draw("A *");
  dedx_reolu1->Write();

  double X2 = myfit->GetChisquare() / myfit->GetNDF();
  cout <<"Chisquare/NDF             =      "<<X2<< endl;
  cout <<"Inter                     =      "<<Inter<< endl;
  cout <<"****************************************"<< endl;
  double X21 = myfit1->GetChisquare() / myfit1->GetNDF();
  cout <<"Chisquare/NDF             =      "<<X21<< endl;
  cout <<"Inter1                    =      "<<Inter1<< endl;
  cout <<"****************************************"<< endl;
  
  out_file->Close();
  cout << "Total events number     : " << total_events << endl;
  cout << "Selected events number      : " << sel_events_tfilter << endl;
  exit(0);
}
